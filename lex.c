/*
* lex.c
*
* Written by : Mayank Bhatt
* Written for: asm85
* Date: July 13, 2013
* Version: 1.0
*/

/**
* Functions for lexical analysis for 8085 assembler
*
* Version 1.0
*
* @Author Mayank Bhatt mayankbh@gmail.com
* @Version 1.0, 07/13/2013
*/

#include <stdio.h>		//Standard header file for I/O
#include <ctype.h>		//Character type testing
#include <string.h>		//String manipulation functions
#include <stdlib.h>		
#include "./tokens.h"		//Contains token information
#include "lex.h"

struct tokenTable tokTab[] = 
{
"EOF", T_EOF,
"EOL", T_EOL,
"IDENT", T_IDENT,
"LABEL", T_LABEL,
"COMMA", T_COMMA,
"SEMICOLON", T_SEMICOLON,
"NUM", T_NUM,
"HEXNUM", T_HEXNUM,
"ERROR", T_ERROR,
"OCTNUM", T_OCTNUM,
"ACI", T_ACI,
"ADC", T_ADC,
"ADD", T_ADD,
"ADI", T_ADI,
"ANA", T_ANA,
"ANI", T_ANI,
"CALL", T_CALL,
"CC", T_CC,
"CM", T_CM,
"CMA", T_CMA,
"CMC", T_CMC,
"CMP", T_CMP,
"CNC", T_CNC,
"CNZ", T_CNZ,
"CP", T_CP,
"CPE", T_CPE,
"CPI", T_CPI,
"CPO", T_CPO,
"CZ", T_CZ,
"DAA", T_DAA,
"DAD", T_DAD,
"DCR", T_DCR,
"DCX", T_DCX,
"DI", T_DI,
"EI", T_EI,
"HLT", T_HLT,
"IN", T_IN,
"INR", T_INR,
"INX", T_INX,
"JC", T_JC,
"JM", T_JM,
"JMP", T_JMP,
"JNC", T_JNC,
"JNZ", T_JNZ,
"JP", T_JP,
"JPE", T_JPE,
"JPO", T_JPO,
"JZ", T_JZ,
"LDA", T_LDA,
"LDAX", T_LDAX,
"LHLD", T_LHLD,
"LXI", T_LXI,
"MOV", T_MOV,
"MVI", T_MVI,
"NOP", T_NOP,
"ORA", T_ORA,
"ORI", T_ORI,
"OUT", T_OUT,
"PCHL", T_PCHL,
"RNZ", T_RNZ,
"POP", T_POP,
"PUSH", T_PUSH,
"RAL", T_RAL,
"RAR", T_RAR,
"RC", T_RC,
"RET", T_RET,
"RIM", T_RIM,
"RLC", T_RLC,
"RM", T_RM,
"RNC", T_RNC,
"RP", T_RP,
"RPE", T_RPE,
"RPO", T_RPO,
"RRC", T_RRC,
"RST", T_RST,
"RZ", T_RZ,
"SBB", T_SBB,
"SBI", T_SBI,
"SHLD", T_SHLD,
"SIM", T_SIM,
"SPHL", T_SPHL,
"STA", T_STA,
"STAX", T_STAX,
"STC", T_STC,
"SUB", T_SUB,
"SUI", T_SUI,
"XCHG", T_XCHG,
"XRA", T_XRA,
"XRI", T_XRI,
"XTHL", T_XTHL,
"A", T_REGA,
"B", T_REGB,
"C", T_REGC,
"D", T_REGD,
"E", T_REGE,
"H", T_REGH,
"L", T_REGL,
"M", T_M,
"PSW", T_PSW,
"SP", T_SP,
"PC", T_PC,
"EQU", T_EQU,
"DB", T_DB,
"DS", T_DS,
"ORG", T_ORG,
NULL, 0
};

void lex(char buffer[], int *buffer_counter,struct token *read_token);		//Lex accepts string and buffer position and returns a token
int validateOctal(char *cp);		//Checks if given number is valid octal
int wordCheck(char *word);	//Checks if word matches with an 8085 mnemonic or register
char * lexDisp(int lexVal);	//Converts a token value to a displayable string
int strcmpi(char *s1, char *s2);	//Case insensitive comparison of two strings
void errorRoutine(char *errorString);	//Prints the error message and exits

/**
* Fetches a new token from buffer
*
* @param buffer, The array containing last line read from file
* @param buffer_counter, Pointer to the present position in buffer
* @param read_token, Pointer to the current token read from buffer
* @return Returns token value and lexeme in read_token
* @author Mayank Bhatt
*/

void lex(char buffer[], int *buffer_counter, struct token *read_token)
{
	char word[MAXTOKSIZE];		//Used to generate tokens
	int word_counter;			//Counter for token
	word_counter = 0;
	while(buffer[*buffer_counter] != '\0')	//Until end of line
	{
		//STATE 0
		word_counter = 0;			//Resetting word counter

		//Checks for identifier/label
		if(isalpha(buffer[*buffer_counter]) || buffer[*buffer_counter] == '_')	//[a-z][A-Z][_]
		{
			//STATE 1
			while(isalnum(buffer[*buffer_counter]) || buffer[*buffer_counter] == '_' && word_counter < MAXTOKSIZE)	//[a-z][A-Z][0-9][_]
				word[word_counter++] = buffer[(*buffer_counter)++];		//Copy character by character

			//Loop exited, non alphanumeric or non underscore character encountered
			if(word_counter >= MAXTOKSIZE)	//Token too long
			{
				read_token->type = T_ERROR;
				return;
			}
			if(buffer[*buffer_counter] == ' ' || buffer[*buffer_counter] == '\t')
			{
				//STATE 3		
				word[word_counter] = '\0';	//Appending null character
				//Skip white spaces
				while(buffer[*buffer_counter] == ' ' || buffer[*buffer_counter] == '\t')
					(*buffer_counter)++;
				//Check if word matches 8085 mnemonic
				read_token->type = wordCheck(word);
				if(read_token->type == T_IDENT)
				{
					strncpy(read_token->label,word, MAXLABELSIZE - 1);
					read_token->label[MAXLABELSIZE - 1] = '\0';
				}
				return;
			}
			if(buffer[*buffer_counter] == ':')
			{
				word[word_counter] = '\0';	//Appending null character
				read_token->type = T_LABEL;
				strncpy(read_token->label,word, MAXLABELSIZE - 1);
				read_token->label[MAXLABELSIZE - 1] = '\0';
				(*buffer_counter)++;		//Moving one character ahead of colon
				return;
			}
			//Now check for comma
			if(buffer[*buffer_counter] == ',')
			{
				//STATE 3
				word[word_counter] = '\0';	//Appending null character
				read_token->type = wordCheck(word);
				if(read_token->type == T_IDENT)
				{
					strncpy(read_token->label,word, MAXLABELSIZE - 1);
					read_token->label[MAXLABELSIZE - 1] = '\0';
				}
				return;

			}
			//Check for comment
			if(buffer[*buffer_counter] == ';' || buffer[*buffer_counter] == '\0')
			{
				word[word_counter] = '\0';	//Appending null character
				read_token->type = wordCheck(word);
				if(read_token->type == T_IDENT)
					strcpy(read_token->label, word);
		
				return;

			}
			//Invalid character encountered if execution reaches here

			read_token->type = T_ERROR;
			return;

			
		}
		//Test for decimal number
		if(isdigit(buffer[*buffer_counter]))		//Decimal, hexadecimal or octal number
		{
			if(buffer[*buffer_counter] != '0')	//Look for decimal number
			{
				while(isdigit(buffer[*buffer_counter])) 	//[[0-9]
					word[word_counter++] = buffer[(*buffer_counter)++];		//Copy character by character
				word[word_counter] = '\0';

				read_token->type = T_NUM;
				strcpy(read_token->label, word);
				return;
			}
			//Look for hexadecimal or octal number
			while(isxdigit(buffer[*buffer_counter])) 	//[[0-9][A-F]
				word[word_counter++] = buffer[(*buffer_counter)++];		//Copy character by character
			//Check if terminates with 'o' or 'h'
			if(buffer[*buffer_counter] == 'h' || buffer[*buffer_counter] == 'H')		//Hexadecimal
			{
				word[word_counter] = '\0';
				(*buffer_counter)++;		//Move to next character in buffer
				read_token->type = T_HEXNUM;
				strcpy(read_token->label, word);
				return;
			}
			//Either octal or error
			if(buffer[*buffer_counter] == 'o' || buffer[*buffer_counter] == 'O')		//Octal
			{
				word[word_counter] = '\0';
				
				if(validateOctal(word))			//Satisfies condition for octal number
				{
					read_token->type = T_OCTNUM;
					(*buffer_counter)++;		//Move to next character in buffer
					strcpy(read_token->label, word);
					return;
				}
				//Otherwise invalid character, treat as error
				read_token->type = T_ERROR;
				return;
			
			}
			//Terminated with non 'o' or 'h' character, treat as error
			read_token->type = T_ERROR;
			return;
		}
		//Test for comma
		if(buffer[*buffer_counter] == ',')
		{
			read_token->type = T_COMMA;
			(*buffer_counter)++;		//Moves to next character
			return;
		}
		//Test for semicolon
		if(buffer[*buffer_counter] == ';')
		{
			read_token->type = T_SEMICOLON;
			return;
		}

		//If non white space, treat as error
		if(!(isblank(buffer[*buffer_counter])))		//Invalid character
		{
			read_token->type = T_ERROR;
			return;
		}
				
		//Otherwise skip white spaces
		while(buffer[*buffer_counter] == ' ' || buffer[*buffer_counter] == '\t')
			(*buffer_counter)++;
		

	}
	//Reached end of line
	read_token->type = T_EOL;
	return;
}

/**
* Checks if read number is a valid octal number
*
* @param cp, The string to be validated
* @return Returns 1 if valid octal number, else 0
* @author Mayank Bhatt
*/

int validateOctal(char *cp)
{
	while((*cp >= '0') && (*cp <= '7'))
		cp++;
	if(*cp == '\0')		//Octal number is valid
		return 1;
	//Not valid
	return 0;
}

/**
* Checks read token against list of reserved words
*
* @param word, Pointer to string to be checked
* @return Returns matched token value if a match is found with a reserved word, else returns T_IDENT
* @author Mayank Bhatt
*/

int wordCheck(char *word)	//Checks word against a list of mnemonics, returns relevant type if found, otherwise returns T_IDENT
{
	int counter = 0;
	
	while(tokTab[counter].tokenName != NULL)	//Until end of table
	{
		if(strcmpi(word, tokTab[counter].tokenName) == 0)	//Match found
			return tokTab[counter].tokenVal;	//Return value
		counter++;
	}
	//No match found, return T_IDENT
	return T_IDENT;	
}

/**
* Accepts a lexed value, checks it in token table, and returns the lexeme
*
* @param lexVal, The lexed token value to be checked, for example T_MOV
* @return Returns a pointer to lexeme corresponding to token value
* @author Mayank Bhatt
*/

char * lexDisp(int lexVal)
{
	int counter = 0;
	
	while(tokTab[counter].tokenName != NULL)	//Until end of table
	{
		if(lexVal == tokTab[counter].tokenVal)	//Match found
			return tokTab[counter].tokenName;	//Return pointer to token name
		counter++;
	}
}

/**
* Case insensitive string comparison routine
*
* @param cp1, The first string
* @param cp2, The second string
* @return Returns 0 if strings are identical, else returns a nonzero value as defined in strcmp(3)
* @author Mayank Bhatt
*/

int strcmpi(char *cp1, char *cp2)
{		
	char *s1 = 0, *s2 = 0;
	int counter = 0;
	int retVal;

	if(s1 = malloc((strlen(cp1) + 1) * sizeof(char)))
		strcpy(s1, cp1);
	else
		errorRoutine("Malloc failed in strcmpi()");

	if(s2 = malloc((strlen(cp2) + 1) * sizeof(char)))
		strcpy(s2, cp2);
	else
		errorRoutine("Malloc failed in strcmpi()");

	while(*(s1 + counter))
	{
		if(*(s1 + counter) >= 'a' && *(s1 + counter) <= 'z')	//Lower case
			*(s1 + counter) -= 32;		//Convert to upper case
		counter++;
	}

	counter = 0;
		
	while(*(s2 + counter))
	{
		if(*(s2 + counter) >= 'a' && *(s2 + counter) <= 'z')	//Lower case
			*(s2 + counter) -= 32;		//Convert to upper case
		counter++;
	}	

	retVal = strcmp(s1, s2);
	free(s1);
	free(s2);
	return retVal;	
}

/**
* Prints an error message and exits the program
*
* @param errorString, The string to be displayed on stdout
* @return N/A
* @author Mayank Bhatt
*/

void errorRoutine(char *errorString)
{
	printf("\n%s", errorString);
	exit(1);
}

