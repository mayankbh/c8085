#include "CPUArch.h"
#include <stdio.h>
#include <stdlib.h>

#define SIGN 0x80
#define ZERO 0x40
#define AUXCARRY 0x10
#define PARITY 0x04
#define CARRY 0x01

#define SETSIGN0 127
#define SETZERO0 191
#define SETAUXCARRY0 239
#define SETPARITY0 251
#define SETCARRY0 254

#define SETSIGN1 0x80
#define SETZERO1 0x40
#define SETAUXCARRY1 0x10
#define SETPARITY1 0x04
#define SETCARRY1 0x01

#define IFSIGN ((AFlags.Reg.Flags & SIGN) == SIGN)	//Performs bitwise AND operation to check SIGN flag
#define IFZERO ((AFlags.Reg.Flags & ZERO) == ZERO)	//Performs bitwise AND operation to check ZERO flag
#define IFAUXCARRY ((AFlags.Reg.Flags & AUXCARRY) == AUXCARRY)	//Performs bitwise AND operation to check AUXCARRY flag
#define IFPARITY ((AFlags.Reg.Flags & PARITY) == PARITY)	//Performs bitwise AND operation to check PARITY flag
#define IFCARRY ((AFlags.Reg.Flags & CARRY) == CARRY)	//Performs bitwise AND operation to check CARRY flag

unsigned char *baseAddress;
unsigned short int programCounter;
unsigned int cycles = 0;
unsigned int states = 0;	

void setZeroFlag();
void setSignFlag();
void setParityFlag();
void setCarryTrue();
void setCarryFalse();
void setAuxCarryTrue();
void setAuxCarryFalse();

void xACI();	//Add with carry immediate
void xADC();	//Add with carry
void xADD();	//Add
void xADI();	//Add immediate
void xANA();	//Logical AND with accumulator
void xANI();	//Logical AND immediate with accumulator
void xCALL();	//Call
void xCC();	//Call if carry
void xCM();	//Call if minus
void xCMA();	//Complement accumulator
void xCMC();	//Complement carry
void xCMP();	//Compare with accumulator
void xCNC();	//Call if no carry
void xCNZ();	//Call if not zero
void xCP();	//Call if positive
void xCPE();	//Call if parity even
void xCPI();	//Compare immediate
void xCPO();	//Call if parity odd
void xCZ();	//Call if zero
void xDAA();	//Decimal adjust accumulator
void xDAD();	//Double register add
void xDCR();	//Decrement
void xDCX();	//Decrement register pair
void xDI();	//Disable interrupts
void xEI();	//Enable interrupts
void xHLT();	//Halt 
void xIN();	//Input from port
void xINR();	//Increment
void xINX();	//Increment register pair
void xJC();	//Jump if carry
void xJM();	//Jump if minus
void xJMP();	//Jump
void xJNC();	//Jump if not carry
void xJNZ();	//Jump if not zero
void xJP();	//Jump if positive
void xJPE();	//Jump if parity even
void xJPO();	//Jump if parity odd
void xJZ();	//Jump if zero
void xLDA();	//Load accumulator direct
void xLDAX();	//Load accumulator indirect
void xLHLD();	//Load H and L direct
void xLXI();	//Load register pair immediate
void xMOV();	//Move
void xMVI();	//Move immediate
void xNOP();	//No operation
void xORA();	//Inclusive OR with accumulator
void xORI();	//Inclusive OR immediate
void xOUT();	//Output to port
void xPCHL();	//Move H&L to program counter
void xPOP();	//Pop
void xPUSH();	//Push
void xRAL();	//Rotate left through carry
void xRAR();	//Rotate right through carry
void xRC();	//Return if carry
void xRET();	//Return
void xRIM();	//Read interrupt mask
void xRLC();	//Rotate accumulator left
void xRM();	//Return if minus
void xRNC();	//Return if no carry
void xRNZ();	//Return if not zero
void xRP();	//Return if positive
void xRPE();	//Return if parity even
void xRPO();	//Return if parity odd
void xRRC();	//Rotate accumulator right
void xRST();	//Reset
void xRZ();	//Return if zero
void xSBB();	//Subtract with borrow
void xSBI();	//Subtract immediate with borrow
void xSHLD();	//Store H and L direct
void xSIM();	//Set interrupt mask
void xSPHL();	//Move H & L to SP
void xSTA();	//Store accumulator direct
void xSTAX();	//Store accumulator indirect
void xSTC();	//Set carry
void xSUB();	//Subtract
void xSUI();	//Subtract immediate
void xXCHG();	//Exchange H and L with D and E
void xXRA();	//Exclusive OR with accumulator
void xXRI();	//Exclusive OR immediate with accumulator
void xXTHL();	//Exchange H & L with top of stack


void *functionTable[] = {	xNOP, xLXI, xSTAX, xINX, xINR, xDCR, xMVI, xRLC, NULL, xDAD,
				xLDAX, xDCX, xINR, xDCR, xMVI, xRRC, NULL, xLXI, xSTAX, xINX, 
				xINR, xDCR, xMVI, xRAL, NULL, xDAD, xLDAX, xDCX, xINR, xDCR, 
				xMVI, xRAR, xRIM, xLXI, xSHLD, xINX, xINR, xDCR, xMVI, xDAA, 
				NULL, xDAD, xLHLD, xDCX, xINR, xDCR, xMVI, xCMA, xSIM, xLXI, 
				xSTA, xINX, xINR, xDCR, xMVI, xSTC, NULL, xDAD, xLDA, xDCX, 
				xINR, xDCR, xMVI, xCMC, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, 
				xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, 
				xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, 
				xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, 
				xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, 
				xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, 
				xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xMOV, xADD, xADD,
				xADD, xADD, xADD, xADD, xADD, xADD, xADC, xADC, xADC, xADC,
				xADC, xADC, xADC, xADC, xSUB, xSUB, xSUB, xSUB, xSUB, xSUB,
				xSUB, xSUB, xSBB, xSBB, xSBB, xSBB, xSBB, xSBB, xSBB, xSBB,
				xANA, xANA, xANA, xANA, xANA, xANA, xANA, xANA, xXRA, xXRA,
				xXRA, xXRA, xXRA, xXRA, xXRA, xXRA, xORA, xORA, xORA, xORA,
				xORA, xORA, xORA, xORA, xCMP, xCMP, xCMP, xCMP, xCMP, xCMP,
				xCMP, xCMP, xRNZ, xPOP, xJNZ, xJMP, xCNZ, xPUSH, xADI, xRST,
				xRZ, xRET, xJZ, NULL, xCZ, xCALL, xACI, xRST, xRNC, xPOP,
				xJNC, xOUT, xCNC, xPUSH, xSUI, xRST, xRC, NULL, xJC, xIN,
				xCC, NULL, xSBI, xRST, xRPO, xPOP, xJPO, xXTHL, xCPO, xPUSH,
				xANI, xRST, xRPE, xPCHL, xJPE, xXCHG, xCPE, NULL, xXRI, xRST,
				xRP, xPOP, xJP, xDI, xCP, xPUSH, xORI, xRST, xRM, xSPHL,
				xJM, xEI, xCM, NULL, xCPI, xRST
			};

void main()
{
	baseAddress = malloc(64000);
}

void setZeroFlag()
{
	if(AFlags.Reg.A == 0)	
		AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;	//Set zero flag to 1
	else
		AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;	//Set zero flag to 0
}

void setSignFlag()
{
	if(AFlags.Reg.Flags & SIGN == SIGN)
		AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;	//Set sign flag to 1
	else
		AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;	//Set sign flag to 0
}

void setParityFlag()
{
	unsigned char temp = AFlags.Reg.A;
	int counter = 0;
	int loopVar = 0;
	while(loopVar < 8)	//Moving from bit 7 to bit 0
	{
		if(temp & 0x80 == 0x80)		//First bit is 1
			counter++;			//Increment counter
		//Run bitshift
		temp = temp << 1;
		loopVar++;
	}
	if(counter % 2 == 0)
	{
		//Set parity flag to 1
		AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
		return;
	}
	//Odd parity, set parity flag to 0
	AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
	return;	
}

void setCarryTrue()
{
	AFlags.Reg.Flags = AFlags.Reg.Flags | SETCARRY1;
}

void setCarryFalse()
{
	AFlags.Reg.Flags = AFlags.Reg.Flags & SETCARRY0;
}

void setAuxCarryTrue()
{
	AFlags.Reg.Flags = AFlags.Reg.Flags | SETAUXCARRY1;
}

void setAuxCarryFalse()
{
	AFlags.Reg.Flags = AFlags.Reg.Flags & SETAUXCARRY0;
}

void xACI()	//Add with carry immediate
{
	unsigned char data;
	data = baseAddress[++programCounter];	//Fetch next byte from memory
	programCounter++;	//Set PC to next instruction
	
	if(IFCARRY)	//If carry flag is set to 1
	{
		cycles += 2;
		states += 7;
		//Set carry flag
		if((unsigned int) AFlags.Reg.A + 1 + data > 0xFF)//Need to set carry flag to true
			setCarryTrue();
		else
			setCarryFalse();
		//Set aux carry flag
		if((AFlags.Reg.A & 0x0F) + (data & 0x0F) + 1 > 0x0F)	//Need to set aux carry to true
			setAuxCarryTrue();
		else
			setAuxCarryFalse();

		AFlags.Reg.A += data + 1;	//Add data + 1 to carry
		//Set other flags
		setZeroFlag();
		setSignFlag();
		setParityFlag();
		return;
	}

	cycles += 2;
	states += 7;

	//Set carry flag
	if((unsigned int) AFlags.Reg.A + data > 0xFF)//Need to set carry flag to true
		setCarryTrue();
	else
		setCarryFalse();
	//Set aux carry flag
	if((AFlags.Reg.A & 0x0F) + (data & 0x0F)  > 0x0F)	//Need to set aux carry to true
		setAuxCarryTrue();
	else
		setAuxCarryFalse();

	AFlags.Reg.A += data ;	//Add data to carry
	//Set other flags
	setZeroFlag();
	setSignFlag();
	setParityFlag();
}

void xADC()	//Add with carry
{
	unsigned char data;
	switch(baseAddress[programCounter] & 0x0F)
	{
		case 0x08:	//Reg B
			data = BC.Reg.B;	//Operand is value stored in register B
			break;
		case 0x09:	//Reg C
			data = BC.Reg.C;	//Operand is value stored in register C
			break;
		case 0x0A:	//Reg D
			data = DE.Reg.D;	//Operand is value stored in register D
			break;
		case 0x0B:	//Reg E
			data = DE.Reg.E;	//Operand is value stored in register E
			break;
		case 0x0C:	//Reg H
			data = HL.Reg.H;	//Operand is value stored in register H
			break;
		case 0x0D:	//Reg L
			data = HL.Reg.L;	//Operand is value stored in register L
			break;
		case 0x0E:	//M
			data = baseAddress[HL.HLPair];	//Operand is value stored in address pointed to by HL pair
			break;
		case 0x0F:	//Reg A
			data = AFlags.Reg.A;	//Operand is value stored in register A
			break;
	}

	programCounter++;	//Set PC to next instruction

	if(IFCARRY)	//If carry flag is set to 1
	{
		cycles += 1;
		states += 4;
		//Set carry flag
		if((unsigned int) AFlags.Reg.A + 1 + data > 0xFF)//Need to set carry flag to true
			setCarryTrue();
		else
			setCarryFalse();
		//Set aux carry flag
		if((AFlags.Reg.A & 0x0F) + (data & 0x0F) + 1 > 0x0F)	//Need to set aux carry to true
			setAuxCarryTrue();
		else
			setAuxCarryFalse();
		AFlags.Reg.A += data + 1;	//Add data + 1 to carry
		//Set other flags
		setZeroFlag();
		setSignFlag();
		setParityFlag();
		return;
	}
	cycles += 1;
	states += 4;
		//Set carry flag
	if((unsigned int) AFlags.Reg.A + data > 0xFF)//Need to set carry flag to true
		setCarryTrue();
	else
		setCarryFalse();
	//Set aux carry flag
	if((AFlags.Reg.A & 0x0F) + (data & 0x0F)  > 0x0F)	//Need to set aux carry to true
		setAuxCarryTrue();
	else
		setAuxCarryFalse();
	AFlags.Reg.A += data ;	//Add data to carry
	//Set other flags
	setZeroFlag();
	setSignFlag();
	setParityFlag();
}

void xADD()	//Add
{
	unsigned char data;
	switch(baseAddress[programCounter] & 0x07)
	{
		case 0x00:	//Reg B
			data = BC.Reg.B;	//Operand is value stored in Reg B
			break;
		case 0x01:	//Reg C
			data = BC.Reg.C;	//Operand is value stored in Reg C
			break;
		case 0x02:	//Reg D
			data = DE.Reg.D;	//Operand is value stored in Reg D
			break;
		case 0x03:	//Reg E
			data = DE.Reg.E;	//Operand is value stored in Reg E
			break;
		case 0x04:	//Reg H
			data = HL.Reg.H;		//Operand is value stored in Reg H
			break;
		case 0x05:	//Reg L
			data = HL.Reg.L;	//Operand is value stored in Reg L
			break;
		case 0x06:	//M
			data = baseAddress[HL.HLPair];	//Operand is value at address pointed to by HL Pair
			break;
		case 0x07:	//Reg A
			data = AFlags.Reg.A;	//Operand is value stored in Reg A
			break;
	}

	programCounter++;	//Set PC to next instruction

	cycles += 1;
	states += 4;
	//Set carry flag
	if((unsigned int) AFlags.Reg.A + data > 0xFF)//Need to set carry flag to true
		setCarryTrue();
	else
		setCarryFalse();
	//Set aux carry flag
	if((AFlags.Reg.A & 0x0F) + (data & 0x0F)  > 0x0F)	//Need to set aux carry to true
		setAuxCarryTrue();
	else
		setAuxCarryFalse();
	AFlags.Reg.A += data ;	//Add data to carry
	//Set other flags
	setZeroFlag();
	setSignFlag();
	setParityFlag();
}

void xADI()	//Add immediate
{
	unsigned char data = baseAddress[++programCounter];

	programCounter++;	//Set PC to next instruction

	cycles += 1;
	states += 4;

	//Set carry flag
	if((unsigned int) AFlags.Reg.A + data > 0xFF)//Need to set carry flag to true
		setCarryTrue();
	else
		setCarryFalse();
	//Set aux carry flag
	if((AFlags.Reg.A & 0x0F) + (data & 0x0F)  > 0x0F)	//Need to set aux carry to true
		setAuxCarryTrue();
	else
		setAuxCarryFalse();
	AFlags.Reg.A += data ;	//Add data to carry
	//Set other flags
	setZeroFlag();
	setSignFlag();
	setParityFlag();
}

void xANA()	//Logical AND with accumulator
{
	unsigned char data;

	switch(baseAddress[programCounter] & 0x0F)
	{
		case 0x00:	//Reg B
			data = BC.Reg.B;	//Operand is value stored in Reg B
			break;
		case 0x01:	//Reg C
			data = BC.Reg.C;	//Operand is value stored in Reg C
			break;
		case 0x02:	//Reg D
			data = DE.Reg.D;	//Operand is value stored in Reg D
			break;
		case 0x03:	//Reg E
			data = DE.Reg.E;	//Operand is value stored in Reg E
			break;
		case 0x04:	//Reg H
			data = HL.Reg.H;	//Operand is value stored in Reg H
			break;
		case 0x05:	//Reg L
			data = HL.Reg.L;	//Operand is value stored in Reg L
			break;
		case 0x06:	//M
			data = baseAddress[HL.HLPair];	//Operand is value at address pointed to by HL Pair
			break;
		case 0x07:	//Reg A
			data = AFlags.Reg.A;	//Operand is value stored in Reg 
			break;
	}
	programCounter++;	//Set PC to next instruction
	cycles += 1;
	states += 4;
	AFlags.Reg.A = AFlags.Reg.A & data;	//Performs bitwise AND operation between accumulator and data
	//Set carry and aux carry flags
	setCarryTrue();
	setAuxCarryTrue();
	//Set other flags
	setZeroFlag();
	setSignFlag();
	setParityFlag();
}

void xANI()	//Logical AND immediate with accumulator
{
	unsigned char data = baseAddress[++programCounter];
	programCounter++;	//Set PC to next instruction

	cycles += 2;
	states += 7;

	AFlags.Reg.A = AFlags.Reg.A & data;	//Performs bitwise AND operation between accumulator and data
	//Set carry and aux carry flags
	setCarryTrue();
	setAuxCarryTrue();
	//Set other flags
	setZeroFlag();
	setSignFlag();
	setParityFlag();
}

void xCALL()	//Call
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));
	programCounter += 2;			//Move two bytes ahead
	SP -= 2;				//Shift stack pointer two bytes back
	*((unsigned short int *)(baseAddress + SP)) = programCounter;	//Pushes address of next instruction onto the stack
	cycles += 5;
	states += 18;

	programCounter = data;			//Move to routine called
}

void xCC()	//Call if carry
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));
	programCounter += 2;			//Move two bytes ahead
	if(IFCARRY)
	{
		SP -= 2;				//Shift stack pointer two bytes back
		*((unsigned short int *)(baseAddress + SP)) = programCounter;	//Pushes address of next instruction onto the stack

		cycles += 5;
		states += 18;

		programCounter = data;			//Move to routine called
		return;
	}
	//Carry flag is false
	cycles += 2;
	states += 9;
	
}

void xCM()	//Call if minus
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));
	programCounter += 2;			//Move two bytes ahead
	if(IFSIGN)	//Sign flag is true if minus
	{
		SP -= 2;				//Shift stack pointer two bytes back
		*((unsigned short int *)(baseAddress + SP)) = programCounter;	//Pushes address of next instruction onto the stack

		cycles += 5;
		states += 18;

		programCounter = data;			//Move to routine called
		return;
	}
	//Sign flag is false
	cycles += 2;
	states += 9;
}

void xCMA()	//Complement accumulator
{
	cycles += 1;
	states += 4;
	programCounter++;	//Set PC to next instruction

	AFlags.Reg.A = ~(AFlags.Reg.A) ;		//Taking one's complement
}

void xCMC()	//Complement carry
{
	cycles += 1;
	states += 4;
	programCounter++;	//Set PC to next instruction

	if(IFCARRY)	//If carry flag is true
	{
		//Set it to false
		AFlags.Reg.Flags = AFlags.Reg.Flags & SETCARRY0;
		return;
	}
	//Carry flag is false
	//Set it to true
	AFlags.Reg.Flags = AFlags.Reg.Flags | SETCARRY1;
	
}

void xCMP()	//Compare with accumulator
{
	unsigned char data;

	switch(baseAddress[programCounter] & 0x0F)
	{
		case 0x08:	//Reg B
			data = BC.Reg.B;	//Operand is value stored in register B
			break;
		case 0x09:	//Reg C
			data = BC.Reg.C;	//Operand is value stored in register C
			break;
		case 0x0A:	//Reg D
			data = DE.Reg.D;	//Operand is value stored in register D
			break;
		case 0x0B:	//Reg E
			data = DE.Reg.E;	//Operand is value stored in register E
			break;
		case 0x0C:	//Reg H
			data = HL.Reg.H;	//Operand is value stored in register H
			break;
		case 0x0D:	//Reg L
			data = HL.Reg.L;	//Operand is value stored in register L
			break;
		case 0x0E:	//M
			data = baseAddress[HL.HLPair];	//Operand is value stored in address pointed to by HL pair
			break;
		case 0x0F:	//Reg A
			data = AFlags.Reg.A;	//Operand is value stored in register A
			break;
	}
	programCounter++;	//Set PC to next instruction

	cycles += 1;
	states += 4;

	data = ~(data) + 1;	//Taking two's complement of data

	if((unsigned int) AFlags.Reg.A + data > 0xFF)//Need to set carry flag to true
		setCarryTrue();
	else
		setCarryFalse();

	if((AFlags.Reg.A & 0x0F) + (data & 0x0F) + 1 > 0x0F)	//Need to set aux carry to true
		setAuxCarryTrue();
	else
		setAuxCarryFalse();

	xCMC();		//Need to complement the carry flag because subtraction has taken place

	AFlags.Reg.A += data;

	setZeroFlag();
	setParityFlag();
	setSignFlag();
}

void xCNC()	//Call if no carry
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));
	programCounter += 2;			//Move two bytes ahead

	if(!(IFCARRY))	//If carry flag is 0
	{
		SP -= 2;				//Shift stack pointer two bytes back
		*((unsigned short int *)(baseAddress + SP)) = programCounter;	//Pushes address of next instruction onto the stack

		cycles += 5;
		states += 18;

		programCounter = data;			//Move to routine called
		return;
	}
	//Sign flag is false
	cycles += 2;
	states += 9;
}

void xCNZ()	//Call if not zero
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));
	programCounter += 2;			//Move two bytes ahead

	if(!(IFZERO))	//If zero flag is set to 0
	{
		SP -= 2;				//Shift stack pointer two bytes back
		*((unsigned short int *)(baseAddress + SP)) = programCounter;	//Pushes address of next instruction onto the stack

		cycles += 5;
		states += 18;

		programCounter = data;			//Move to routine called
		return;
	}
	//Sign flag is false
	cycles += 2;
	states += 9;
}

void xCP()	//Call if positive
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));
	programCounter += 2;			//Move two bytes ahead

	if(!(IFSIGN))	//Sign flag is 0 if positive
	{
		SP -= 2;				//Shift stack pointer two bytes back
		*((unsigned short int *)(baseAddress + SP)) = programCounter;	//Pushes address of next instruction onto the stack

		cycles += 5;
		states += 18;

		programCounter = data;			//Move to routine called
		return;
	}
	//Sign flag is false
	cycles += 2;
	states += 9;
}

void xCPE()	//Call if parity even
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));	//Address of routine to be called
	programCounter += 2;			//Move two bytes ahead

	if(IFPARITY)	//If parity flag is set to 1
	{
		SP -= 2;				//Shift stack pointer two bytes back
		*((unsigned short int *)(baseAddress + SP)) = programCounter;	//Pushes address of next instruction onto the stack

		cycles += 5;
		states += 18;

		programCounter = data;			//Move to routine called
		return;
	}
	//Parity flag is 0
	cycles += 2;
	states += 9;
}

void xCPI()	//Compare immediate
{
	unsigned char data = baseAddress[++programCounter];	//Fetch next byte

	cycles += 1;
	states += 4;
	programCounter++;	//Set PC to next instruction

	data = ~(data) + 1;	//Taking two's complement of data

	if((unsigned int) AFlags.Reg.A + data > 0xFF)//Need to set carry flag to true
		setCarryTrue();
	else
		setCarryFalse();

	if((AFlags.Reg.A & 0x0F) + (data & 0x0F) + 1 > 0x0F)	//Need to set aux carry to true
		setAuxCarryTrue();
	else
		setAuxCarryFalse();

	xCMC();		//Need to complement the carry flag because subtraction has taken place

	AFlags.Reg.A += data;

	setZeroFlag();
	setParityFlag();
	setSignFlag();
}

void xCPO()	//Call if parity odd
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));	//Address of routine to be called
	programCounter += 2;			//Move two bytes ahead

	if(!(IFPARITY))	//If parity flag is set to 0
	{
		SP -= 2;				//Shift stack pointer two bytes back
		*((unsigned short int *)(baseAddress + SP)) = programCounter;	//Pushes address of next instruction onto the stack

		cycles += 5;
		states += 18;

		programCounter = data;			//Move to routine called
		return;
	}
	//Parity flag is 1
	cycles += 2;
	states += 9;
}

void xCZ()	//Call if zero
{

	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));	//Address of routine to be called
	programCounter += 2;			//Move two bytes ahead

	if(IFZERO)	//If zero flag is set to 0
	{
		SP -= 2;				//Shift stack pointer two bytes back
		*((unsigned short int *)(baseAddress + SP)) = programCounter;	//Pushes address of next instruction onto the stack

		cycles += 5;
		states += 18;

		programCounter = data;			//Move to routine called
		return;
	}
	//Sign flag is false
	cycles += 2;
	states += 9;
}

//Incomplete function below
void xDAA()	//Decimal adjust accumulator
{
	programCounter++;	//Set PC to next instruction
}
//Incomplete function above
// +---+---+---+---+---+---+---+---+
// | X | X | R | P | X | X | X | X |
// +---+---+---+---+---+---+---+---+

void xDAD()	//Double register add
{
	unsigned short int data;

	cycles += 3;
	states += 10;
	programCounter++;	//Set PC to next instruction

	switch(baseAddress[programCounter] & 0x30)
	{
		case 0x00:	//DAD B
			data = BC.BCPair;
			break;
		case 0x10:	//DAD D
			data = DE.DEPair;
			break;
		case 0x20:	//DAD H
			data = HL.HLPair;
			break;	
		case 0x30:	//DAD SP
			data = SP;
			break;	

	}
	if((unsigned int) HL.HLPair + data > 0xFFFF)	//Checking carry condition
		setCarryTrue();
	else
		setCarryFalse();
	HL.HLPair += data;
}


//Need to clean up below function, use pointers instead of entire segments in switch() statement
void xDCR()	//Decrement
{
	unsigned char data2 = ~(0x01) + 1;	//Two's complement of 1
	unsigned int loopVar = 0;
	unsigned int counter = 0;
	unsigned char temp;

	switch(baseAddress[programCounter] & 0x38)
	{
		case 0x00:	//DCR B
			cycles += 1;
			states += 5;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((BC.Reg.B & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			BC.Reg.B += data2;	//Subtracting 1;
			temp = BC.Reg.B;
			
			//Set zero flag
			if(BC.Reg.B == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(BC.Reg.B & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		case 0x08:	//DCR C
			cycles += 1;
			states += 5;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((BC.Reg.C & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			BC.Reg.C += data2;	//Subtracting 1;
			temp = BC.Reg.C;
			
			//Set zero flag
			if(BC.Reg.C == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(BC.Reg.C & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		case 0x10:	//DCR D
			cycles += 1;
			states += 5;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((DE.Reg.D & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			DE.Reg.D += data2;	//Subtracting 1;
			temp = DE.Reg.D;
			
			//Set zero flag
			if(DE.Reg.D == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(DE.Reg.D & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		case 0x18:	//DCR E
			cycles += 1;
			states += 5;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((DE.Reg.E & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			DE.Reg.E += data2;	//Subtracting 1;
			temp = DE.Reg.E;
			
			//Set zero flag
			if(DE.Reg.E == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(DE.Reg.E & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		case 0x20:	//DCR H
			cycles += 1;
			states += 5;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((HL.Reg.H & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			HL.Reg.H += data2;	//Subtracting 1;
			temp = HL.Reg.H;
			
			//Set zero flag
			if(HL.Reg.H == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(HL.Reg.H & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		case 0x28:	//DCR L
			cycles += 1;
			states += 5;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((HL.Reg.L & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			HL.Reg.L += data2;	//Subtracting 1;
			temp = HL.Reg.L;
			
			//Set zero flag
			if(HL.Reg.L == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(HL.Reg.L & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		case 0x30:	//DCR M
			cycles += 3;
			states += 10;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((baseAddress[HL.HLPair] & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			baseAddress[HL.HLPair] += data2;	//Subtracting 1;
			temp = baseAddress[HL.HLPair];
			
			//Set zero flag
			if(baseAddress[HL.HLPair] == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(baseAddress[HL.HLPair] & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		case 0x38:	//DCR A
			cycles += 1;
			states += 5;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((AFlags.Reg.A & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			AFlags.Reg.A += data2;	//Subtracting 1;
			temp = AFlags.Reg.A;
			
			//Set zero flag
			if(AFlags.Reg.A == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(AFlags.Reg.A & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		
	}

}

void xDCX()	//Decrement register pair
{
	switch(baseAddress[programCounter] & 0x30)
	{
		case 0x00:	//DCX B
			BC.BCPair--;
			break;
		case 0x10:	//DCX D
			DE.DEPair--;
			break;
		case 0x20:	//DCX H
			HL.HLPair--;
			break;
		case 0x30:	//DCX SP
			SP--;
			break;
	}
	programCounter++;	//Set PC to next instruction
	cycles += 1;
	states += 6;
}

void xDI()	//Disable interrupts
{
	cycles += 1;
	states += 4;
	programCounter++;	//Set PC to next instruction

}


void xEI()	//Enable interrupts
{
	cycles += 1;
	states += 4;
	programCounter++;	//Set PC to next instruction

}

void xHLT()	//Halt 
{
	cycles += 1;
	states += 5;
	programCounter++;	//Set PC to next instruction
}

void xIN()	//Input from port
{
	cycles += 3;
	states += 10;
	programCounter++;	//Set PC to next instruction
}

void xINR()	//Increment
{

	unsigned char data2 = 0x01;	//1 to be added
	unsigned int loopVar = 0;
	unsigned int counter = 0;
	unsigned char temp;

	switch(baseAddress[programCounter] & 0x38)
	{
		case 0x00:	//DCR B
			cycles += 1;
			states += 5;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((BC.Reg.B & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			BC.Reg.B += data2;	//Subtracting 1;
			temp = BC.Reg.B;
			
			//Set zero flag
			if(BC.Reg.B == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(BC.Reg.B & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		case 0x08:	//DCR C
			cycles += 1;
			states += 5;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((BC.Reg.C & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			BC.Reg.C += data2;	//Subtracting 1;
			temp = BC.Reg.C;
			
			//Set zero flag
			if(BC.Reg.C == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(BC.Reg.C & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		case 0x10:	//DCR D
			cycles += 1;
			states += 5;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((DE.Reg.D & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			DE.Reg.D += data2;	//Subtracting 1;
			temp = DE.Reg.D;
			
			//Set zero flag
			if(DE.Reg.D == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(DE.Reg.D & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		case 0x18:	//DCR E
			cycles += 1;
			states += 5;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((DE.Reg.E & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			DE.Reg.E += data2;	//Subtracting 1;
			temp = DE.Reg.E;
			
			//Set zero flag
			if(DE.Reg.E == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(DE.Reg.E & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		case 0x20:	//DCR H
			cycles += 1;
			states += 5;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((HL.Reg.H & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			HL.Reg.H += data2;	//Subtracting 1;
			temp = HL.Reg.H;
			
			//Set zero flag
			if(HL.Reg.H == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(HL.Reg.H & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		case 0x28:	//DCR L
			cycles += 1;
			states += 5;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((HL.Reg.L & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			HL.Reg.L += data2;	//Subtracting 1;
			temp = HL.Reg.L;
			
			//Set zero flag
			if(HL.Reg.L == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(HL.Reg.L & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		case 0x30:	//DCR M
			cycles += 3;
			states += 10;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((baseAddress[HL.HLPair] & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			baseAddress[HL.HLPair] += data2;	//Subtracting 1;
			temp = baseAddress[HL.HLPair];
			
			//Set zero flag
			if(baseAddress[HL.HLPair] == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(baseAddress[HL.HLPair] & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		case 0x38:	//DCR A
			cycles += 1;
			states += 5;
			programCounter++;	//Set PC to next instruction
			//Set aux carry flag
			if((AFlags.Reg.A & 0x0F) + (data2 & 0x0F) > 0x0F)
				setAuxCarryTrue();
			else
				setAuxCarryFalse();
			AFlags.Reg.A += data2;	//Subtracting 1;
			temp = AFlags.Reg.A;
			
			//Set zero flag
			if(AFlags.Reg.A == 0)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETZERO1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETZERO0;
			//Set sign flag
			if(AFlags.Reg.A & SIGN == SIGN)
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETSIGN1;
			else
				AFlags.Reg.Flags = AFlags.Reg.Flags & SETSIGN0;
			//Set parity flags
			while(loopVar < 8)	//Moving from bit 7 to bit 0
			{
				if(temp & 0x80 == 0x80)		//First bit is 1
					counter++;			//Increment counter
				//Run bitshift
				temp = temp << 1;
				loopVar++;
			}
			if(counter % 2 == 0)
			{
				//Set parity flag to 1
				AFlags.Reg.Flags = AFlags.Reg.Flags | SETPARITY1;
				return;
			}
			//Odd parity, set parity flag to 0
			AFlags.Reg.Flags = AFlags.Reg.Flags & SETPARITY0;
			return;	
			break;
		
	}
}

void xINX()	//Increment register pair
{
	switch(baseAddress[programCounter] & 0x30)
	{
		case 0x00:	//DCX B
			BC.BCPair++;
			break;
		case 0x10:	//DCX D
			DE.DEPair++;
			break;
		case 0x20:	//DCX H
			HL.HLPair++;
			break;
		case 0x30:	//DCX SP
			SP++;
			break;
	}
	cycles += 1;
	states += 6;
	programCounter++;	//Set PC to next instruction
}

void xJC()	//Jump if carry
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));	//Fetching 2 bytes of data from memory
	programCounter += 2;		///Moving PC two bytes ahead, onto next instruction
	
	if(IFCARRY)
	{
		programCounter = data;	//Set program counter to value read from memory
		cycles += 3;
		states += 10;
		return;
	}
	//Carry flag is not 1
	cycles += 2;
	states += 7;
}

void xJM()	//Jump if minus
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));	//Fetching 2 bytes of data from memory
	programCounter += 2;		///Moving PC two bytes ahead, onto next instruction
	
	if(IFSIGN)
	{
		programCounter = data;	//Set program counter to value read from memory
		cycles += 3;
		states += 10;
		return;
	}
	//Sign flag is not 1
	cycles += 2;
	states += 7;
}

void xJMP()	//Jump
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));	//Fetching 2 bytes of data from memory
	programCounter += 2;		//Moving PC two bytes ahead, onto next instruction
	
	cycles += 3;
	states += 10;
	
	programCounter = data;		//Set program counter to value read from memory
}

void xJNC()	//Jump if not carry
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));	//Fetching 2 bytes of data from memory
	programCounter += 2;		///Moving PC two bytes ahead, onto next instruction
	
	if(!(IFCARRY))
	{
		programCounter = data;	//Set program counter to value read from memory
		cycles += 3;
		states += 10;
		return;
	}
	//Carry flag is not 0
	cycles += 2;
	states += 7;
}

void xJNZ()	//Jump if not zero
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));	//Fetching 2 bytes of data from memory
	programCounter += 2;		///Moving PC two bytes ahead, onto next instruction
	
	if(!(IFZERO))
	{
		programCounter = data;	//Set program counter to value read from memory
		cycles += 3;
		states += 10;
		return;
	}
	//Zero flag is not 0
	cycles += 2;
	states += 7;
}

void xJP()	//Jump if positive
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));	//Fetching 2 bytes of data from memory
	programCounter += 2;		///Moving PC two bytes ahead, onto next instruction
	
	if(!(IFSIGN))
	{
		programCounter = data;	//Set program counter to value read from memory
		cycles += 3;
		states += 10;
		return;
	}
	//Sign flag is not 0
	cycles += 2;
	states += 7;
}

void xJPE()	//Jump if parity even
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));	//Fetching 2 bytes of data from memory
	programCounter += 2;		///Moving PC two bytes ahead, onto next instruction
	
	if(IFPARITY)
	{
		//Even parity
		programCounter = data;	//Set program counter to value read from memory
		cycles += 3;
		states += 10;
		return;
	}
	//Odd parity
	cycles += 2;
	states += 7;
}

void xJPO()	//Jump if parity odd
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));	//Fetching 2 bytes of data from memory
	programCounter += 2;		///Moving PC two bytes ahead, onto next instruction
	
	if(!(IFPARITY))
	{
		//Even parity
		programCounter = data;	//Set program counter to value read from memory
		cycles += 3;
		states += 10;
		return;
	}
	//Even parity
	cycles += 2;
	states += 7;
}

void xJZ()	//Jump if zero
{
	programCounter++;
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));	//Fetching 2 bytes of data from memory
	programCounter += 2;		///Moving PC two bytes ahead, onto next instruction
	
	if(IFZERO)
	{
		programCounter = data;	//Set program counter to value read from memory
		cycles += 3;
		states += 10;
		return;
	}
	//Zero flag is not 0
	cycles += 2;
	states += 7;
}

void xLDA()	//Load accumulator direct
{
	programCounter++;		//Move to next byte
	unsigned short int data = *((unsigned short int *) (baseAddress + programCounter));	//Load address into data variable
	programCounter += 2;

	cycles += 4;
	states += 13;

	AFlags.Reg.A = baseAddress[data];	//Load value stored at memory address into accumulator
}

void xLDAX()	//Load accumulator indirect
{
	unsigned short int data;
	switch(baseAddress[programCounter] & 0x10)
	{
		case 0x00:	//LDAX B
			data = BC.BCPair;
			break;
		case 0x10:	//LDAX D
			data = DE.DEPair;
			break;
	}

	cycles += 2;
	states += 7;
	programCounter++;	//Set PC to next instruction

	AFlags.Reg.A = baseAddress[data];
}

void xLHLD()	//Load H and L direct
{
	unsigned short int *data;
	programCounter++;
	data = ((unsigned short int *) (baseAddress + programCounter));	//Load address into data variable
	programCounter += 2;

	cycles += 5;
	states += 16;
	HL.HLPair = *data;
}

void xLXI()	//Load register pair immediate
{
	unsigned short int data;
	unsigned short int *ptr;
	//Load address of register pair to be changed into ptr
	switch(baseAddress[programCounter] & 0x30)
	{
		case 0x00:	//LXI B
			ptr = &(BC.BCPair);
			break;
		case 0x01:	//LXI D
			ptr = &(DE.DEPair);
			break;
		case 0x10:	//LXI H
			ptr = &(HL.HLPair);
			break;
		case 0x11:	//LXI SP
			ptr = &(SP);
			break;
	}
	programCounter++;
	data = *((unsigned short int *) (baseAddress + programCounter));	//Loading two bytes into data variab
	programCounter += 2;

	*ptr = data;

	cycles += 3;
	states += 10;
}

void xMOV()	//Move
{
	unsigned char *reg1, *reg2;	//reg1 is destination, reg2 is source
	
	switch(baseAddress[programCounter] & 0x38)
	{
		case 0x00:
			reg1 = &(BC.Reg.B);
			break;
		case 0x08:
			reg1 = &(BC.Reg.C);
			break;
		case 0x10:
			reg1 = &(DE.Reg.D);
			break;
		case 0x18:
			reg1 = &(DE.Reg.E);
			break;
		case 0x20:
			reg1 = &(HL.Reg.H);
			break;
		case 0x28:
			reg1 = &(HL.Reg.L);
			break;
		case 0x30:
			reg1 = baseAddress + HL.HLPair;
			break;
		case 0x38:
			reg1 = &(AFlags.Reg.A);
			break;
	}

	switch(baseAddress[programCounter] & 0x07)
	{
		case 0x00:
			reg2 = &(BC.Reg.B);
			break;
		case 0x01:
			reg2 = &(BC.Reg.C);
			break;
		case 0x02:
			reg2 = &(DE.Reg.D);
			break;
		case 0x03:
			reg2 = &(DE.Reg.E);
			break;
		case 0x04:
			reg2 = &(HL.Reg.H);
			break;
		case 0x05:
			reg2 = &(HL.Reg.L);
			break;
		case 0x06:
			reg2 = baseAddress + HL.HLPair;
			break;
		case 0x07:
			reg2 = &(AFlags.Reg.A);
			break;
	}
	
	//Now move data from *reg2 to *reg1	
	*reg1 = *reg2;

	if(reg2 == baseAddress + HL.HLPair || reg1 == baseAddress + HL.HLPair)
	{
		cycles += 2;
		states += 7;
		return;
	}

	cycles += 1;
	states += 4;
	programCounter++;	//Set PC to next instruction
}

void xMVI()	//Move immediate
{
	unsigned char data;
	unsigned char *reg1;	//Destination register
	switch(baseAddress[programCounter] & 0x38)
	{
		case 0x00:
			reg1 = &(BC.Reg.B);
			break;
		case 0x08:
			reg1 = &(BC.Reg.C);
			break;
		case 0x10:
			reg1 = &(DE.Reg.D);
			break;
		case 0x18:
			reg1 = &(DE.Reg.E);
			break;
		case 0x20:
			reg1 = &(HL.Reg.H);
			break;
		case 0x28:
			reg1 = &(HL.Reg.L);
			break;
		case 0x30:
			reg1 = baseAddress + HL.HLPair;
			break;
		case 0x38:
			reg1 = &(AFlags.Reg.A);
			break;
	}
	programCounter++;
	data = baseAddress[programCounter];

	*reg1 = data;

	if(reg1 == baseAddress + HL.HLPair)
	{
		cycles += 3;
		states += 10;
		return;
	}
	cycles += 2;
	states += 7;
	programCounter++;	//Set PC to next instruction
}

void xNOP()	//No operation
{
	//Do nothing
	programCounter++;
}

void xORA()	//Inclusive OR with accumulator
{
	unsigned char data;
	switch(baseAddress[programCounter] & 0x07)
	{
		case 0x00:	//Reg B
			data = BC.Reg.B;	//Operand is value stored in Reg B
			break;
		case 0x01:	//Reg C
			data = BC.Reg.C;	//Operand is value stored in Reg C
			break;
		case 0x02:	//Reg D
			data = DE.Reg.D;	//Operand is value stored in Reg D
			break;
		case 0x03:	//Reg E
			data = DE.Reg.E;	//Operand is value stored in Reg E
			break;
		case 0x04:	//Reg H
			data = HL.Reg.H;		//Operand is value stored in Reg H
			break;
		case 0x05:	//Reg L
			data = HL.Reg.L;	//Operand is value stored in Reg L
			break;
		case 0x06:	//M
			data = baseAddress[HL.HLPair];	//Operand is value at address pointed to by HL Pair
			break;
		case 0x07:	//Reg A
			data = AFlags.Reg.A;	//Operand is value stored in Reg A
			break;

	}
	programCounter++;	//Set PC to next instruction
	setCarryFalse();
	setAuxCarryFalse();
	AFlags.Reg.A = AFlags.Reg.A | data;	//Performing bitwise OR operation
	if(baseAddress[programCounter] & 0x07 == 0x06)	//ORA M
	{
		setZeroFlag();
		setSignFlag();
		setParityFlag();
		cycles += 2;
		states += 7;
		return;
	}
	//Set remaining flags
	setZeroFlag();
	setSignFlag();
	setParityFlag();
	cycles += 1;
	states += 4;
}

void xORI()	//Inclusive OR immediate
{
	unsigned char data;
	data = baseAddress[++programCounter];

	AFlags.Reg.A = AFlags.Reg.A | data;	//Performing bitwise OR operation
	
	programCounter++;	//Set PC to next instruction
	setCarryFalse();
	setAuxCarryFalse();
	setZeroFlag();
	setSignFlag();
	setParityFlag();
	cycles += 2;
	states += 7;
}

void xOUT()	//Output to port
{
	cycles += 3;
	states += 10;
	programCounter++;	//Set PC to next instruction
}	

void xPCHL()	//Move H&L to program counter
{
	programCounter = HL.HLPair;
	cycles += 1;
	states += 6;
}

void xPOP()	//Pop
{
	switch(baseAddress[programCounter] & 0x30)
	{
		case 0x00:	//POP B
			BC.BCPair = *(unsigned short int *) (baseAddress + SP);		//Load value at address pointed to by SP into BC pair
			SP += 2;	//Increment stack pointer by 2
			break;
		case 0x01:	//POP D
			DE.DEPair = *(unsigned short int *) (baseAddress + SP);		//Load value at address pointed to by SP into BC pair
			SP += 2;	//Increment stack pointer by 2
			break;
		case 0x10:	//POP H
			HL.HLPair = *(unsigned short int *) (baseAddress + SP);		//Load value at address pointed to by SP into BC pair
			SP += 2;	//Increment stack pointer by 2
			break;
		case 0x11:	//POP PSW
			AFlags.PSW = *(unsigned short int *) (baseAddress + SP);		//Load value at address pointed to by SP into BC pair
			SP += 2;	//Increment stack pointer by 2
			break;
	}
	programCounter++;	//Set PC to next instruction
	cycles += 3;
	states += 10;
}


void xPUSH()	//Push
{
	SP -= 2;
	switch(baseAddress[programCounter] & 0x30)
	{
		case 0x00:	//PUSH B
			*((unsigned int *)(baseAddress + SP)) = BC.BCPair;
			break;
		case 0x01:	//PUSH D
			*((unsigned int *)(baseAddress + SP)) = DE.DEPair;
			break;
		case 0x10:	//PUSH H
			*((unsigned int *)(baseAddress + SP)) = HL.HLPair;
			break;
		case 0x11:	//PUSH PSW
			*((unsigned int *)(baseAddress + SP)) = AFlags.PSW;
			break;
	}
	cycles += 3;
	states += 13;
	programCounter++;	//Set PC to next instruction
}

void xRAL()	//Rotate left through carry
{
	char carryFlag = 0, bitSevenFlag = 0;	//Flags to check carry and bit 7, respectively
	if(IFCARRY)
		carryFlag = 1;

	if((AFlags.Reg.A & 0x80) == 0x80)	//If bit 7 is set to 1
	
		bitSevenFlag = 1;
	

	AFlags.Reg.A = AFlags.Reg.A << 1;	//Performing bitshift
	
	if(carryFlag == 1)
		AFlags.Reg.A = AFlags.Reg.A | 0x01;	//Set bit 0 to true
	else
		AFlags.Reg.A  =AFlags.Reg.A & 0xFE;	//Set bit 0 to false

	if(bitSevenFlag == 1)
		setCarryTrue();
	else
		setCarryFalse();

	cycles += 1;
	states += 4;
	programCounter++;	//Set PC to next instruction
}

void xRAR()	//Rotate right through carry
{
	char carryFlag = 0, bitZeroFlag = 0;	//Flags to check carry and bit 7, respectively
	if(IFCARRY)
		carryFlag = 1;

	if((AFlags.Reg.A & 0x01) == 0x01)	//If bit 0 is set to 1
	{
		bitZeroFlag = 1;
	}

	AFlags.Reg.A = AFlags.Reg.A >> 1;	//Performing bitshift
	
	if(carryFlag == 1)
		AFlags.Reg.A = AFlags.Reg.A | 0x80;	//Set bit 7 to true
	else
		AFlags.Reg.A  =AFlags.Reg.A & 0x7F;	//Set bit 7 to false

	if(bitZeroFlag == 1)
		setCarryTrue();
	else
		setCarryFalse();

	cycles += 1;
	states += 4;
	programCounter++;	//Set PC to next instruction
}

void xRC()	//Return if carry
{
	if(IFCARRY)	//If carry flag is set to true
	{
		cycles += 3;
		states += 12;
		programCounter = *((unsigned short int *) (baseAddress + SP));	//Pop value from stack and store in program counter
		SP += 2;		//Increment stack pointer by two
		return;
	}
	//No carry
	cycles += 1;
	states += 6;	
	programCounter++;	//Set PC to next instruction
}

void xRET()	//Return
{
	cycles += 3;
	states += 10;
	programCounter = *((unsigned short int *) (baseAddress + SP));	//Pop value from stack and store in program counter
	SP += 2;		//Increment stack pointer by two
}

void xRIM()	//Read interrupt mask
{
	//Pending
	cycles += 1;
	states += 4;
	programCounter++;	//Set PC to next instruction
}

void xRLC()	//Rotate accumulator left
{
	char bitSevenFlag = 0;
	if((AFlags.Reg.A & 0x80) == 0x80)	//Bit 7 is true
		bitSevenFlag = 1;

	AFlags.Reg.A = AFlags.Reg.A << 1;	//Perform bitshift
	if(bitSevenFlag == 1)
	{
		AFlags.Reg.A = AFlags.Reg.A | 0x01;	//Set bit 0 to true
		setCarryTrue();
	}
	else
	{
		AFlags.Reg.A = AFlags.Reg.A | 0xFE;	//Set bit 0 to false
		setCarryFalse();
	}
	cycles += 1;
	states += 4;
	programCounter++;	//Set PC to next instruction
}

void xRM()	//Return if minus
{
	if(IFSIGN)	//If sign flag is true, implying minus
	{
		cycles += 3;
		states += 12;
		programCounter = *((unsigned short int *) (baseAddress + SP));	//Pop value from stack and store in program counter
		SP += 2;		//Increment stack pointer by two
		return;
	}
	//Not minus
	cycles += 1;
	states += 6;
	programCounter++;	//Set PC to next instruction
}

void xRNC()	//Return if no carry
{
	if(!(IFCARRY))	//If carry flag is false
	{
		cycles += 3;
		states += 12;
		programCounter = *((unsigned short int *) (baseAddress + SP));	//Pop value from stack and store in program counter
		SP += 2;		//Increment stack pointer by two
		return;
	}
	//Carry flag is true
	cycles += 1;
	states += 6;
	programCounter++;	//Set PC to next instruction
}
	
void xRNZ()	//Return if not zero
{
	if(!(IFZERO))	//If zero flag is false
	{
		cycles += 3;
		states += 12;
		programCounter = *((unsigned short int *) (baseAddress + SP));	//Pop value from stack and store in program counter
		SP += 2;		//Increment stack pointer by two
		return;
	}
	//Zero flag is true
	cycles += 1;
	states += 6;
	programCounter++;	//Set PC to next instruction
}

void xRP()	//Return if positive
{
	if(!(IFSIGN))	//If sign flag is false
	{
		cycles += 3;
		states += 12;
		programCounter = *((unsigned short int *) (baseAddress + SP));	//Pop value from stack and store in program counter
		SP += 2;		//Increment stack pointer by two
		return;
	}
	//Sign flag is true
	cycles += 1;
	states += 6;
	programCounter++;	//Set PC to next instruction
}

void xRPE()	//Return if parity even
{
	if(IFPARITY)	//If parity flag is true, implying even parity
	{
		cycles += 3;
		states += 12;
		programCounter = *((unsigned short int *) (baseAddress + SP));	//Pop value from stack and store in program counter
		SP += 2;		//Increment stack pointer by two
		return;
	}
	//Parity flag is false
	cycles += 1;
	states += 6;
	programCounter++;	//Set PC to next instruction
}

void xRPO()	//Return if parity odd
{
	if(!(IFPARITY))	//If parity flag is false, implying odd parity
	{
		cycles += 3;
		states += 12;
		programCounter = *((unsigned short int *) (baseAddress + SP));	//Pop value from stack and store in program counter
		SP += 2;		//Increment stack pointer by two
		return;
	}
	//Parity flag is true
	cycles += 1;
	states += 6;
	programCounter++;	//Set PC to next instruction
}

void xRRC()	//Rotate accumulator right
{
	char bitZeroFlag = 0;
	if((AFlags.Reg.A & 0x01) == 0x01)	//Bit 0 is true
		bitZeroFlag = 1;

	AFlags.Reg.A = AFlags.Reg.A >> 1;	//Perform bitshift
	if(bitZeroFlag == 1)
	{
		AFlags.Reg.A = AFlags.Reg.A | 0x80;	//Set bit 7 to true
		setCarryTrue();
	}
	else
	{
		AFlags.Reg.A = AFlags.Reg.A & 0x7F;	//Set bit 7 to false
		setCarryFalse();
	}
	cycles += 1;
	states += 4;
	programCounter++;	//Set PC to next instruction
}

void xRST()	//Reset
{
	//Incomplete
	programCounter++;	//Set PC to next instruction
}

void xRZ()	//Return if zero
{
	if(IFZERO)	//If zero flag is true
	{
		cycles += 3;
		states += 12;
		programCounter = *((unsigned short int *) (baseAddress + SP));	//Pop value from stack and store in program counter
		SP += 2;		//Increment stack pointer by two
		return;
	}
	//Zero flag is false
	cycles += 1;
	states += 6;
	programCounter++;	//Set PC to next instruction
}

void xSBB()	//Subtract with borrow
{
	unsigned char data;
	switch(baseAddress[programCounter] & 0x07)
	{
		case 0x00:	//SBB B
			data = BC.Reg.B;
			break;
		case 0x01:	//SBB C
			data = BC.Reg.C;
			break;
		case 0x02:	//SBB D
			data = DE.Reg.D;
			break;
		case 0x03:	//SBB E
			data = DE.Reg.E;
			break;
		case 0x04:	//SBB H
			data = HL.Reg.H;
			break;
		case 0x05:	//SBB L
			data = HL.Reg.L;
			break;
		case 0x06:	//SBB M
			data = baseAddress[HL.HLPair];
			break;
		case 0x07:	//SBB A
			data = AFlags.Reg.A;
			break;
	}

	programCounter++;	//Set PC to next instruction
	data = ~(data) + 1;	//Taking two's complement
	if(IFCARRY)	//If carry flag is true
		data = data + 1;	//Adding carry flag to data

	if((unsigned int) AFlags.Reg.A + data > 0xFF)
		setCarryTrue();
	else
		setCarryFalse();

	if((AFlags.Reg.A & 0x0F) + (data & 0x0F) > 0x0F)
		setAuxCarryTrue();
	else
		setAuxCarryFalse();

	AFlags.Reg.A = AFlags.Reg.A + data;

	if(baseAddress[programCounter] & 0x07 == 0x06)	//SBB M
	{
		cycles += 2;
		states += 7;
		setZeroFlag();
		setParityFlag();
		setSignFlag();
		return;
	}

	cycles += 1;
	states += 4;
	setZeroFlag();
	setParityFlag();
	setSignFlag();
}

void xSBI()	//Subtract immediate with borrow
{
	unsigned char data = baseAddress[++programCounter];
	data = ~(data) + 1;	//Taking two's complement
	if(IFCARRY)	//If carry flag is true
		data = data + 1;	//Adding carry flag to data

	if((unsigned int) AFlags.Reg.A + data > 0xFF)
		setCarryTrue();
	else
		setCarryFalse();

	if((AFlags.Reg.A & 0x0F) + (data & 0x0F) > 0x0F)
		setAuxCarryTrue();
	else
		setAuxCarryFalse();

	AFlags.Reg.A = AFlags.Reg.A + data;

	programCounter++;	//Set PC to next instruction
	cycles += 2;
	states += 7;
	setZeroFlag();
	setSignFlag();
	setParityFlag();
}

void xSHLD()	//Store H and L direct
{
	programCounter++;
	unsigned short int *data = ((unsigned short int *) (baseAddress + programCounter));	//Storing pointer to label
	programCounter += 2;
	
	cycles += 5;
	states += 16;

	*data = HL.HLPair;	//Storing H and L pair to memory
}

void xSIM()	//Set interrupt mask
{
	//Pending
	programCounter++;	//Set PC to next instruction
}

void xSPHL()	//Move H & L to SP
{
	programCounter++;
	SP = HL.HLPair;
}

void xSTA()	//Store accumulator direct
{
	programCounter++;
	unsigned char *data = (baseAddress + programCounter);	//Storing pointer to label
	programCounter += 2;
	
	cycles += 4;
	states += 13;
	programCounter++;	//Set PC to next instruction

	*data = AFlags.Reg.A;	//Storing accumulator to memory
}

void xSTAX()	//Store accumulator indirect
{
	unsigned short int *ptr;
	switch(baseAddress[programCounter] & 0x10)
	{
		case 0x00:	//STAX B
			ptr = &(BC.BCPair);
			break;
		case 0x10:	//STAX D
			ptr = &(DE.DEPair);
			break;
	}
	programCounter++;	//Set PC to point to next instruction
	cycles += 2;
	states += 7;

	baseAddress[*ptr] = AFlags.Reg.A;	//Storing accumulator to memory
}

void xSTC()	//Set carry
{
	setCarryTrue();
	programCounter++;	//Set PC to next instruction
}

void xSUB()	//Subtract
{
	unsigned char data;
	switch(baseAddress[programCounter] & 0x07)
	{
		case 0x00:	//SBB B
			data = BC.Reg.B;
			break;
		case 0x01:	//SBB C
			data = BC.Reg.C;
			break;
		case 0x02:	//SBB D
			data = DE.Reg.D;
			break;
		case 0x03:	//SBB E
			data = DE.Reg.E;
			break;
		case 0x04:	//SBB H
			data = HL.Reg.H;
			break;
		case 0x05:	//SBB L
			data = HL.Reg.L;
			break;
		case 0x06:	//SBB M
			data = baseAddress[HL.HLPair];
			break;
		case 0x07:	//SBB A
			data = AFlags.Reg.A;
			break;
	}
	programCounter++;	//Set PC to next instruction
	data = ~(data) + 1;	//Taking two's complement

	if((unsigned int) AFlags.Reg.A + data > 0xFF)
		setCarryTrue();
	else
		setCarryFalse();

	if((AFlags.Reg.A & 0x0F) + (data & 0x0F) > 0x0F)
		setAuxCarryTrue();
	else
		setAuxCarryFalse();

	AFlags.Reg.A = AFlags.Reg.A + data;

	if(baseAddress[programCounter] & 0x07 == 0x06)	//SBB M
	{
		cycles += 2;
		states += 7;
		setZeroFlag();
		setParityFlag();
		setSignFlag();
		return;
	}

	cycles += 1;
	states += 4;
	setZeroFlag();
	setParityFlag();
	setSignFlag();
}

void xSUI()	//Subtract immediate
{
	unsigned char data = baseAddress[++programCounter];
	data = ~(data) + 1;	//Taking two's complement

	if((unsigned int) AFlags.Reg.A + data > 0xFF)
		setCarryTrue();
	else
		setCarryFalse();

	if((AFlags.Reg.A & 0x0F) + (data & 0x0F) > 0x0F)
		setAuxCarryTrue();
	else
		setAuxCarryFalse();

	AFlags.Reg.A = AFlags.Reg.A + data;

	programCounter++;	//Set PC to next instruction
	cycles += 2;
	states += 7;
	setZeroFlag();
	setSignFlag();
	setParityFlag();
}

void xXCHG()	//Exchange H and L with D and E
{
	unsigned short int temp;
	programCounter++;	//Set PC to next instruction
	cycles += 1;
	states += 4;
	temp = HL.HLPair;
	HL.HLPair = DE.DEPair;
	DE.DEPair = temp;
}

void xXRA()	//Exclusive OR with accumulator
{
	unsigned char data;
	switch(baseAddress[programCounter] & 0x07)
	{
		case 0x00:	//Reg B
			data = BC.Reg.B;	//Operand is value stored in Reg B
			break;
		case 0x01:	//Reg C
			data = BC.Reg.C;	//Operand is value stored in Reg C
			break;
		case 0x02:	//Reg D
			data = DE.Reg.D;	//Operand is value stored in Reg D
			break;
		case 0x03:	//Reg E
			data = DE.Reg.E;	//Operand is value stored in Reg E
			break;
		case 0x04:	//Reg H
			data = HL.Reg.H;		//Operand is value stored in Reg H
			break;
		case 0x05:	//Reg L
			data = HL.Reg.L;	//Operand is value stored in Reg L
			break;
		case 0x06:	//M
			data = baseAddress[HL.HLPair];	//Operand is value at address pointed to by HL Pair
			break;
		case 0x07:	//Reg A
			data = AFlags.Reg.A;	//Operand is value stored in Reg A
			break;

	}
	programCounter++;	//Set PC to next instruction
	setCarryFalse();
	setAuxCarryFalse();
	AFlags.Reg.A = AFlags.Reg.A ^ data;	//Performing bitwise XOR operation
	if(baseAddress[programCounter] & 0x07 == 0x06)	//ORA M
	{
		setZeroFlag();
		setSignFlag();
		setParityFlag();
		cycles += 2;
		states += 7;
		return;
	}
	//Set remaining flags
	setZeroFlag();
	setSignFlag();
	setParityFlag();
	cycles += 1;
	states += 4;
}

void xXRI()	//Exclusive OR immediate with accumulator
{
	unsigned char data;
	data = baseAddress[++programCounter];

	AFlags.Reg.A = AFlags.Reg.A ^ data;	//Performing bitwise XOR operation
	
	programCounter++;	//Set PC to next instruction
	setCarryFalse();
	setAuxCarryFalse();
	setZeroFlag();
	setSignFlag();
	setParityFlag();
	cycles += 2;
	states += 7;
}

void xXTHL()	//Exchange H & L with top of stack
{
	unsigned short int *ptr = (unsigned short int *) (baseAddress + SP);	//Pointer to top of stack
	unsigned short int temp;

	programCounter++;	//Set PC to next instruction

	temp = HL.HLPair;
	HL.HLPair = *ptr;
	*ptr = temp;
}
