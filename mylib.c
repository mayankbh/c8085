unsigned long int stringToNumber(char *string, int radix)
{
	int counter = 0;
	unsigned long int num = 0;

	while(string[counter] != '\0')
	{
		if(string[counter] >= 'A' && string[counter] <= 'F')
		{
			num = (num * radix) + (string[counter] - 'A' + 10);
			counter++;
			continue;
		}
		//Not [A-F]
		num = (num * radix) + (string[counter] - '0');
		counter++;
	}
	return num;
}
