#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "tokens.h"
#include "symTab.h"
#include "lex.h"

//Declare symbol table as a global array, all entries intially null

struct symbolTableEntry symbolTable[SYMBOLTABLESIZE];

void symbolTableCall(struct token read_token, int programCounter);
void strupr(char *string);
int labelNotDefined(char *label, unsigned int hashVal);	//Checks label under hash value, returns 0 if not used, 1 if used but not defined, 2 if used and defined (error case)
unsigned int hash(char *string);	//Accepts a string, generates hash from it
int hashCheck(unsigned int hashVal);	//Checks hash table for existing entry, returns 1 if no entry found, else 0
void createNewEntry(char *label, unsigned int hashVal, int entryType, int programCounter);	//Creates first entry under given hash value
void hashAppend(char *label, unsigned int hashVal, int entryType, int programCounter);	//Appends entry under given hash value
void forwardRefAppend(char *label, unsigned int hashVal, int programCounter);
void addressPatch(char *label, unsigned int hashVal, int programCounter);
void symInit(struct symbolTableEntry *symTab, int tableSize);	
void symPrint(struct symbolTableEntry *symTab, int tableSize);
int symUnresolved(struct symbolTableEntry *symTab, int tableSize, int toPrint);	//Determines number of unresolved entries in symbol table


void symbolTableCall(struct token read_token, int programCounter)
{
	int hashVal;
	struct symbolTableEntry *traverse;

	switch(read_token.type)
	{
	//If it is a label
		case T_LABEL:
			//Generate hash
			hashVal = hash(read_token.label);
			//Check if the generated hash already exists in symbol table
			if(hashCheck(hashVal))
			{
				//Hash already in use, check if label is in use.
				switch(labelNotDefined(read_token.label, hashVal))
				{
					case 0: 	//Not in use
						//Append entry in hash table, set defined flag to true
						hashAppend(read_token.label, hashVal, LABEL, programCounter);
	//The following statement is for testing only
						programCounter+=2;	
	//The preceding statement is for testing only
						break;
					case 1:		//In use, but not defined
					//Set defined flag to true, patch all addresses
						addressPatch(read_token.label, hashVal, programCounter);
						break;
					case 2:		//In use, already defined
					//Label already defined, generate error and move to next token
						printf("\nLabel %s already defined.", read_token.label);
						break;
				}	
				break;	//Move to next token
			}	
			//Hash not in use, create new entry here
			createNewEntry(read_token.label, hashVal, LABEL, programCounter);
	//The following statement is for testing only
			programCounter+=2;	
	//The preceding statement is for testing only
			break;
		case T_IDENT:
		//Generate hashs
			hashVal = hash(read_token.label);
			//Check if the generated hash already exists in symbol table
			if(hashCheck(hashVal))
			{
				//Hash already in use, check if label is in use
				switch(labelNotDefined(read_token.label, hashVal))
				{
					case 0:		//Not in use
						//Create entry, append to forward references
						hashAppend(read_token.label, hashVal, IDENT, programCounter);
						break;
					case 1:		//Name used but label not defined
						//Add to forward references
						forwardRefAppend(read_token.label, hashVal, programCounter);
						break;
					case 2:		//Label defined previously
						//Patch address to memory directly
						traverse = &symbolTable[hashVal];
						while(strcmpi(read_token.label, traverse->name) != 0)
							traverse = traverse->sLink;
						spit16((unsigned short int) traverse->address, programCounter);	//Fix needed here
						break;
				}
				break;	//Next token
			}
			//Hash not in use, create new entry here
			createNewEntry(read_token.label, hashVal, IDENT, programCounter);
			break;
	}
}

void strupr(char *string)	//Converts string to upper case
{
	char *sp = string;
	while(*sp != '\0')
	{
		if(*sp >= 'a' && *sp <= 'z')
			*sp -= 32;
		sp++;
	}
}

unsigned int hash(char *string)
{
	char *cp = string;
	unsigned int hashVal = 0;

	while(*cp != '\0')	//Traverse the string
	{
		hashVal = (hashVal * 65599) + *cp;
		cp++;
	}
	return (hashVal % SYMBOLTABLESIZE);
}


int hashCheck(unsigned int hashVal)
{
	if(symbolTable[hashVal].name[0] == '\0')	//Hash value not in use
		return 0;
	//Hash value in use
	return 1;
}


int labelNotDefined(char *label, unsigned int hashVal)	//Checks label under hash value, returns 0 if not used, 1 if used but not defined, 2 if used and defined (error case)
{
	struct symbolTableEntry *search;

	search = &symbolTable[hashVal];		//Address of first entry under hash value
	//This function is only called if hash collision occurs, implying first entry cannot be null
	//Hence no need to check if first entry is null
	while(search != NULL)	//Until end of linked list
	{
		if(strcmp(search->name, label) == 0)	//Match found
		{
			switch(search->defined)
			{
				case 0:		//Not defined previously
					return 1;
				case 1:		//Defined previously
					return 2;
			}
		}
		//Match not found, move to next element in list
		search = search->sLink;
	}
	//Entire list traversed, no match found. First occurrence of symbol
	return 0;
}

void createNewEntry(char *label, unsigned int hashVal, int entryType, int programCounter)
{
	struct forwardRefs *temp;

	strcpy(symbolTable[hashVal].name, label);	//Set name of entry
	switch(entryType)
	{
		case LABEL:
			symbolTable[hashVal].address = programCounter;	//Set address of symbol table entry
			symbolTable[hashVal].defined = 1;	//Set defined flag to true
			break;
		case IDENT:
			symbolTable[hashVal].defined = 0;
			symbolTable[hashVal].address = 0x00;	//Temporary 0 address
			temp = malloc(sizeof(struct forwardRefs));
			temp->addressToPatch = programCounter;	
			temp->nLink = NULL;	
			symbolTable[hashVal].fLink = temp;
			break;
	}
}

void hashAppend(char *label, unsigned int hashVal, int entryType, int programCounter)
{
	struct symbolTableEntry *traverse;	//Used to traverse linked list
	struct symbolTableEntry *temp;		//Used to append entry to linked list
	struct forwardRefs *fRef;		//Used in case of ident to generate forward reference

	traverse = &symbolTable[hashVal];	//Address of first entry under hash value
	while(traverse->sLink != NULL)			//Until end of linked list
		traverse = traverse->sLink;
	//traverse now points to last element of linked list
	temp = malloc(sizeof(struct symbolTableEntry));
	traverse->sLink = temp;		//Appending latest element to linked list
	temp->sLink = NULL;		//Terminating null pointer
	strcpy(temp->name, label);
	switch(entryType)
	{
		case LABEL:
			temp->address = programCounter;
			temp->defined = 1;
			temp->fLink = NULL;
			break;
		case IDENT:
			temp->address = 0x00;	//Temporary 0 address
			temp->defined = 0;

			fRef = malloc(sizeof(struct forwardRefs));
			fRef->addressToPatch = programCounter;
			fRef->nLink = NULL;
			temp->fLink = fRef;
			break;
	}
}

void addressPatch(char *label, unsigned int hashVal, int programCounter)
{
	struct symbolTableEntry *traverse;
	struct forwardRefs *fRef;

	traverse = &symbolTable[hashVal];
	while(strcmp(label, traverse->name) != 0)
	{
		traverse = traverse->sLink;	//Shift to next entry
	}
	//traverse now points to the entry to be changed
	traverse->address = programCounter;	//Setting address
	traverse->defined = 1;			//Set defined flag to true
	fRef = traverse->fLink;
	while(fRef != NULL)
	{
		//Patch address
		spit16(traverse->address, fRef->addressToPatch);
		//Move to next element
		fRef = fRef->nLink;
	}		
}

void forwardRefAppend(char *label, unsigned int hashVal, int programCounter)
{
	struct symbolTableEntry *traverse = &symbolTable[hashVal];
	struct forwardRefs *fRef, *temp;

	while(strcmp(traverse->name, label) != 0)
		traverse = traverse->sLink;
	//Traverse now points to entry where forward reference must be added
	fRef = traverse->fLink;

	while(fRef->nLink != NULL)
		fRef = fRef->nLink;

	temp = malloc(sizeof(struct forwardRefs));
	temp->addressToPatch = programCounter;
	temp->nLink = NULL;
	
	fRef->nLink = temp;
}

void symInit(struct symbolTableEntry *symTab, int tableSize)
{
	int counter = 0;
	while(counter < tableSize)
	{
		symTab[counter].name[0] = '\0';
		symTab[counter].address = (unsigned int) NULL;
		symTab[counter].defined = (unsigned int) NULL;
		symTab[counter].sLink = (struct symbolTableEntry *) NULL;
		symTab[counter].fLink = (struct forwardRefs *) NULL;
		counter++;	
	}
}

//Function to display symbol table


int symUnresolved(struct symbolTableEntry *symTab, //Pointer to symbol table
			int tableSize, 		//Size of symbol table
			int toPrint)		//Whether to print unresolved entries or not
{
	int counter = 0;
	int undefCount = 0;	//Number of undefined entries
	struct forwardRefs *fRef;	//Used to traverse forward references
	struct symbolTableEntry *symEnt;	//Used to traverse all entries under given hash

	while(counter < tableSize)
	{
		if(symTab[counter].name[0] != '\0')	//Found an existing hashs
		{
			//Check if first entry is defined
			if(symTab[counter].defined == 0)	//Not defined
			{
				undefCount++;
				if(toPrint)	//Check flag
				{
					printf("\n%s referenced at \t", symTab[counter].name);
					fRef = symTab[counter].fLink;
					while(fRef != NULL)
					{
						printf("%04X ", fRef->addressToPatch);
						fRef = fRef->nLink;
					}
				}
			}				
			//Check rest of entries
			symEnt = symTab[counter].sLink;	//Points to next entry
			while(symEnt != NULL)
			{

				if(symEnt->defined == 0)	//Not defined
				{
					undefCount++;
					if(toPrint)	//Check flag
					{
						printf("\n%s referenced at \t", symEnt->name);
						fRef = symEnt->fLink;
						while(fRef != NULL)
						{
							printf("%04X ", fRef->addressToPatch);
							fRef = fRef->nLink;
						}
					}
				}				
				symEnt = symEnt->sLink;
			}
		}
		counter++;
	}	
	if(toPrint)
		printf("\n");
	return undefCount;
}

void symPrint(struct symbolTableEntry *symTab,	
			int tableSize)
{
	int counter = 0;
	struct symbolTableEntry *symEnt;	//Used to traverse all entries under given hash

	while(counter < tableSize)
	{
		if(symTab[counter].name[0] != '\0')	//Found an existing hashs
		{
			printf("\n%s\t\t%04X\t%s", symTab[counter].name, symTab[counter].address, symTab[counter].defined ? "" : "Undefined");
			//Check rest of entries
			symEnt = symTab[counter].sLink;	//Points to next entry
			while(symEnt != NULL)
			{
				printf("\n%s\t\t%04X\t%s", symEnt->name, symEnt->address, symEnt->defined ? "" : "Undefined");
				symEnt = symEnt->sLink;
			}
		}
		printf("\n---\n");
		counter++;
	}	
	printf("\n");

}
