#ifndef CPUARCH
#define CPUARCH

union
{
	unsigned short int PSW;		//A + Flags pair
	struct
	{
		unsigned char Flags;	//Lower byte
		unsigned char A;	//Higher byte	
	} Reg;
} AFlags;

union
{
	unsigned short int BCPair;	//Register pair
	struct
	{
		unsigned char C;	//Lower byte
		unsigned char B;	//Higher byte
	} Reg;
} BC;


union
{
	unsigned short int DEPair;	//Register pair
	struct
	{
		unsigned char E;	//Lower byte
		unsigned char D;	//Higher byte
	} Reg;
} DE;


union
{
	unsigned short int HLPair;	//Register pair
	struct
	{
		unsigned char L;	//Lower byte
		unsigned char H;	//Higher byte
	} Reg;
} HL;

unsigned short int SP;			//16 bit stack pointer

unsigned short int programCounter;	//16 bit program counter

#endif
