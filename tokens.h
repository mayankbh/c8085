#ifndef TOKENS
#define TOKENS

/*
* tokens.h
*
* Written by : Mayank Bhatt
* Written for: asm85
* Date: July 13, 2013
* Version: 1.0
*/

/**
* Token definitions for 8085 assembler
*
* Version 1.0
*
* @Author Mayank Bhatt mayankbh@gmail.com
* @Version 1.0, 07/13/2013
*/


#define T_EOF 0
#define T_EOL 1
#define T_IDENT 2
#define T_LABEL 3
#define T_COMMA 4
#define T_SEMICOLON 5
#define T_NUM 6
#define T_HEXNUM 7
#define T_ERROR 8
#define T_OCTNUM 9


#define T_REGA 	100
#define T_REGB	101
#define T_REGC	102
#define T_REGD	103
#define T_REGE	104
#define T_REGH	105
#define T_REGL	106
#define T_M	107
#define T_PSW	108
#define T_SP	109
#define T_PC	110

#define T_EQU 120
#define T_DB 121
#define T_DS 122
#define T_ORG 123


//Instructions with no operand, generate single byte	
#define T_NOP	201
#define T_RLC	202
#define T_RRC	203
#define T_RAL	204
#define T_RAR	205
#define T_RIM	206
#define T_DAA	207
#define T_CMA	208
#define T_SIM	209
#define T_STC	210
#define T_CMC	211
#define T_RNZ	212
#define T_RZ	213
#define T_RNC	214
#define T_RC	215
#define T_RPO	216
#define T_XTHL	217
#define T_RPE	218
#define T_RP	219
#define T_DI	220
#define T_RM	221
#define T_SPHL	222
#define T_EI	223
#define T_HLT	224
#define T_PCHL	225
#define T_XCHG	226
#define T_RET	227
//Instructions with one operand, generate single byte	
#define T_STAX	229
#define T_INX	230
#define T_INR	231
#define T_DCR	232
#define T_DAD	233
#define T_LDAX	234
#define T_DCX	235
#define T_ADC	236
#define T_ADD	237
#define T_SUB	238
#define T_SBB	239
#define T_ANA	240
#define T_XRA	241
#define T_ORA	242
#define T_CMP	243
#define T_POP	244
#define T_PUSH	245
#define T_RST	246
//Instructions with two operands, generate single byte	
#define T_MOV	248
//Instructions with one operard, generate two bytes	
#define T_ADI	250
#define T_ACI	251
#define T_OUT	252
#define T_SUI	253
#define T_IN	254
#define T_SBI	255
#define T_ANI	256
#define T_XRI	257
#define T_ORI	258
#define T_CPI	259
//Instructions with two operands, generate two bytes	
#define T_MVI	261
//Instructions with one operand, generate three bytes	
#define T_STA	263
#define T_LDA	264
#define T_JNZ	265
#define T_JMP	266
#define T_CNZ	267
#define T_JZ	268
#define T_CZ	269
#define T_CALL	270
#define T_JNC	271
#define T_CNC	272
#define T_JC	273
#define T_CC	274
#define T_JPO	275
#define T_CPO	276
#define T_JPE	277
#define T_CPE	278
#define T_JP	279
#define T_CP	280
#define T_JM	281
#define T_CM	282
#define T_SHLD	283
#define T_LHLD	284
//Instructions with two operands, generate three bytes	
#define T_LXI	286

#endif
