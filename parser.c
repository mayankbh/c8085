#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "tokens.h"
#include "lex.h"


#define EQUENTRYSIZE 30		//Maximum length of entry in EQU table
#define EQUTABLESIZE 500	//Size of EQU table
#define TOKENSTACKSIZE 5	//Maximum size of token stack
#define MEMORYSIZE 0xFFFF	//Size of memory, in bytes

unsigned char *baseAddress;	//Base address for memory

struct EQUTableEntry
{
	char name[EQUENTRYSIZE];	//Identifier name
	int value;			//Value
};

struct EQUTableEntry EQUTable[EQUTABLESIZE];	//Declaring EQU Table as an array of type EQUTableEntry

struct token tokenStack[TOKENSTACKSIZE];	//Token stack is implemented using array
int tokenStackTop = -1;				//Used to track the top of the stack, -1 indicates empty stack

int errorCounter = 0;	//Global counter used to track errors, initialized to zero at the beginning

int EQUIndex = 0;	//Used for EQU table

int programCounter = 0;
int line_counter = 0;

void parseState1(char *buffer, int *buffer_counter, struct token *read_token);	//T_LABEL at beginning of line
void parseState2(char *buffer, int *buffer_counter, struct token *read_token);	//T_DB processing
void parseState3(char *buffer, int *buffer_counter, struct token *read_token);	//T_DB number sequence processing
void parseState4(char *buffer, int *buffer_counter, struct token *read_token);	//T_DS processing
void parseState5(char *buffer, int *buffer_counter, struct token *read_token);	//T_ORG processing
void parseState6(char *buffer, int *buffer_counter, struct token *read_token);	//Expects T_EOL and T_SEMICOLON
void parseState7(char *buffer, int *buffer_counter, struct token *read_token, char *EQUStorageName);	//Test for T_EQU
void parseState8(char *buffer, int *buffer_counter, struct token *read_token, char *EQUStorageName);	//Test for number after T_EQU
void parseState101A(char *buffer, int *buffer_counter, struct token *read_token);	//After T_STAX or T_LDAX
void parseState101B(char *buffer, int *buffer_counter, struct token *read_token);	//After T_INX, T_DAD, T_DCX
void parseState101C(char *buffer, int *buffer_counter, struct token *read_token);	//After T_PUSH, T_POP
void parseState101D(char *buffer, int *buffer_counter, struct token *read_token);	//After T_INR, T_DCR
void parseState102(char *buffer, int *buffer_counter, struct token *read_token);	//After T_RST
void parseState103(char *buffer, int *buffer_counter, struct token *read_token);	//After T_ADC, T_ADD, T_SUB, T_SBB, T_ANA, T_XRA, T_ORA, T_CMP
void parseState103A(char *buffer, int *buffer_counter, struct token *read_token);	//After T_MOV
void parseState104(char *buffer, int *buffer_counter, struct token *read_token);	//T_REGy after T_MOV, T_REGx, T_COMMA
void parseState105(char *buffer, int *buffer_counter, struct token *read_token);	//Expects ADR16
void parseState105A(char *buffer, int *buffer_counter, struct token *read_token);	//Expects D8
void parseState106(char *buffer, int *buffer_counter, struct token *read_token);	//After T_LXI
void parseState107(char *buffer, int *buffer_counter, struct token *read_token);	//After T_MVI

int isOpcode(int tokenType);	//Tests if given token is an opcode
void typeOneProcessing(struct token *read_token);
void opcodeProcessing(char *buffer, int *buffer_counter, struct token *read_token);
void setAtEQUIndex(char *EQULabel, char *EQUNameStorage, int type);
void tokenStackPush(struct token read_token);	//Pushes the token into the stack
int tokenStackPop();					//Pops the token out of the stack, returns the token type of popped token
void spit8(unsigned char output, int programCounter);	//Writes one byte to memory
void spit16(unsigned short int output, int programCounter);
void dumpMemoryToFile(FILE *destination);

void main(int argc, char *argv[])
{
	FILE *inputFile;			//Used to read from text file
	FILE *destination;			//Used to write to binary file
	char buffer[BUFFERSIZE];
	int buffer_counter;			//Counter used to traverse line of text
//	int line_counter;
	struct token read_token;		//Token read from string
	unsigned int hashVal;
	unsigned int errorCount = 0;		//Counter to store the number of errors encountered
	char EQUNameStorage[EQUENTRYSIZE];	//Temporary array used to retain the EQU entry name

	baseAddress = (unsigned char *) malloc(MEMORYSIZE);	//Allocating 64k of memory
	if(baseAddress == NULL)
	{
		//Error in memory allocation
		printf("\nError in allocating memory. Exiting");
		exit(1);
	}
	//Memory allocated fine, proceed

	if(argc != 2)
	{
		printf("Usage:%s  <Input file name>\n", argv[0]);
		exit(1);
	}
	inputFile = fopen(argv[1], "r");	//Opening file in read mode
	if(inputFile == NULL)			//Error in opening file
	{
		printf("Error in opening file %s. Exiting.\n", argv[1]);
		exit(1);				//Exit program
	}

	//Initializing counters

	buffer_counter = 0;			
	line_counter = 0;

	//Reading from file
	
	while(fgets(buffer,BUFFERSIZE,inputFile) != NULL)		//Until end of file
	{
		line_counter++;			//New line starts here, increment counter
		buffer_counter = 0;		//New line, reset buffer counter here
		while(buffer[buffer_counter] != '\n' && buffer[buffer_counter] != '\r')
			buffer_counter++;
		buffer[buffer_counter] = '\0';		//Remove \n and \r
		buffer_counter = 0;
		strupr(buffer);		//Convert entire line to upper case
		do
		{
			//STATE 0
			//
			//Fetch a token from the file
			lex(buffer, &buffer_counter, &read_token);
			//Behaviour based on type of token
			switch(read_token.type)
			{
			//If it is a label
				case T_LABEL:
						//STATE 1
						symbolTableCall(read_token, programCounter);
						lex(buffer, &buffer_counter, &read_token);	//Get next token
						parseState1(buffer, &buffer_counter, &read_token);	//State for T_LABEL processing
						break;	//Move to next token
				case T_ORG:
						lex(buffer, &buffer_counter, &read_token);	//Get next token
						parseState5(buffer, &buffer_counter, &read_token);
						break;
						
				case T_DB:
						lex(buffer, &buffer_counter, &read_token);	//Get next token
						parseState2(buffer, &buffer_counter, &read_token);	//State for T_DB processing
						break;
				case T_DS:
						lex(buffer, &buffer_counter, &read_token);	//Get next token
						parseState4(buffer, &buffer_counter, &read_token);	//State for T_DS processing
						break;
				case T_ERROR:
						errorCount++;
						printf("\nParsing error encountered in line %d", line_counter);
						break;
				case T_EQU:
						errorCount++;
						printf("\nParsing error encountered in line %d", line_counter);
						break;
						
				case T_IDENT:
						strcpy(EQUNameStorage, read_token.label);		//Copies name of token into EQUNameStorage
						lex(buffer, &buffer_counter, &read_token);
						parseState7(buffer, &buffer_counter, &read_token, EQUNameStorage);	//Expects T_EQU
						break;	//Move to next token
				case T_NUM:
						errorCount++;
						read_token.type = T_ERROR;
						break;
				case T_HEXNUM:
						errorCount++;
						read_token.type = T_ERROR;
						break;
					
				case T_OCTNUM:
						errorCount++;
						read_token.type = T_ERROR;
						break;
				case T_COMMA:
						errorCounter++;
						read_token.type = T_ERROR;
						break;
				case T_EOL:
						//Do nothing
						break;	
				case T_SEMICOLON:
						//Do nothing
						break;
				default:
						//If none of the above, must be T_OPCODE type
						//To be defined
						opcodeProcessing(buffer, &buffer_counter, &read_token);
						//To be defined
			}
			
		}	
		while (read_token.type != T_EOL && read_token.type != T_ERROR && read_token.type != T_SEMICOLON);
		//T_EOL or T_EOF or t_SEMICOLON encountered
		if(read_token.type == T_ERROR)	//Error encountered while reading line
			printf("\nError encountered in line %d", line_counter);
	}
	//T_EOF encountered
	printf("\n");
	
	destination = fopen("assembled", "wb");

	if(destination == NULL)
	{
		printf("\nError in opening destination file. Exiting");
		free(baseAddress);
		exit(1);
	}

	dumpMemoryToFile(destination);	//Write memory to file
	//Be sure to free all the memory allocated during execution
	free(baseAddress);
	//deleteSymbolTable();
}

void typeOneHandling(struct token *read_token)
{
	switch(read_token->type)
	{
		case T_NOP:
			spit8(0x00, programCounter);
			programCounter++;
			break;
		case T_RLC:
			spit8(0x07, programCounter);
			programCounter++;
			break;
		case T_RRC:
			spit8(0x0F, programCounter);
			programCounter++;
			break;
		case T_RAL:
			spit8(0x17, programCounter);
			programCounter++;
			break;
		case T_RAR:
			spit8(0x1F, programCounter);
			programCounter++;
			break;
		case T_RIM:
			spit8(0x20, programCounter);
			programCounter++;
			break;
		case T_DAA:
			spit8(0x27, programCounter);
			programCounter++;
			break;
		case T_CMA:
			spit8(0x2F, programCounter);
			programCounter++;
			break;
		case T_SIM:
			spit8(0x30, programCounter);
			programCounter++;
			break;
		case T_STC:
			spit8(0x37, programCounter);
			programCounter++;
			break;
		case T_CMC:
			spit8(0x3F, programCounter);
			programCounter++;
			break;
		case T_RNZ:
			spit8(0xC0, programCounter);
			programCounter++;
			break;
		case T_RZ:
			spit8(0xC8, programCounter);
			programCounter++;
			break;
		case T_RNC:
			spit8(0xD0, programCounter);
			programCounter++;
			break;
		case T_RC:
			spit8(0xD8, programCounter);
			programCounter++;
			break;
		case T_RPO:
			spit8(0xE0, programCounter);
			programCounter++;
			break;
		case T_XTHL:
			spit8(0xE3, programCounter);
			programCounter++;
			break;
		case T_RPE:
			spit8(0xE8, programCounter);
			programCounter++;
			break;
		case T_RP:
			spit8(0xF0, programCounter);
			programCounter++;
			break;
		case T_DI:
			spit8(0xF3, programCounter);
			programCounter++;
			break;
		case T_RM:
			spit8(0xF8, programCounter);
			programCounter++;
			break;
		case T_SPHL:
			spit8(0xF9, programCounter);
			programCounter++;
			break;
		case T_EI:
			spit8(0xFB, programCounter);
			programCounter++;
			break;
		case T_HLT:
			spit8(0x76, programCounter);
			programCounter++;
			break;
		case T_PCHL:
			spit8(0xE9, programCounter);
			programCounter++;
			break;
		case T_XCHG:
			spit8(0xEB, programCounter);
			programCounter++;
			break;
		case T_RET:
			spit8(0xC9, programCounter);
			programCounter++;
			break;
	}
}

void typeTwoHandling(char *buffer, int *buffer_counter, struct token *read_token)
{
	switch(read_token->type)
	{
		case T_STAX:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState101A(buffer, buffer_counter, read_token);
			break;
		case T_INX:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState101B(buffer, buffer_counter, read_token);
			break;
		case T_INR:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState101D(buffer, buffer_counter, read_token);
			break;
		case T_DCR:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState101D(buffer, buffer_counter, read_token);
			break;
		case T_DAD:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState101B(buffer, buffer_counter, read_token);
			break;
		case T_LDAX:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState101A(buffer, buffer_counter, read_token);
			break;
		case T_DCX:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState101B(buffer, buffer_counter, read_token);
			break;
		case T_ADC:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState103(buffer, buffer_counter, read_token);
			break;
		case T_ADD:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState103(buffer, buffer_counter, read_token);
			break;
		case T_SUB:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState103(buffer, buffer_counter, read_token);
			break;
		case T_SBB:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState103(buffer, buffer_counter, read_token);
			break;
		case T_ANA:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState103(buffer, buffer_counter, read_token);
			break;
		case T_XRA:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState103(buffer, buffer_counter, read_token);
			break;
		case T_ORA:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState103(buffer, buffer_counter, read_token);
			break;
		case T_CMP:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState103(buffer, buffer_counter, read_token);
			break;
		case T_POP:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState101C(buffer, buffer_counter, read_token);
			break;
		case T_PUSH:
			tokenStackPush(*read_token);	//Push token into stack
			lex(buffer, buffer_counter, read_token);
			parseState101C(buffer, buffer_counter, read_token);
			break;
		case T_RST:
			lex(buffer, buffer_counter, read_token);
			parseState102(buffer, buffer_counter, read_token);
			break;
		default:
			//Unexpected token, treat as error
			read_token->type = T_ERROR;
			return;
	}
}

void typeThreeHandling(char *buffer, int *buffer_counter, struct token *read_token)
{
	lex(buffer, buffer_counter, read_token);
	parseState103A(buffer, buffer_counter, read_token);
}

void typeFourHandling(char *buffer, int *buffer_counter, struct token *read_token)
{
	switch(read_token->type)
	{
		case T_ADI:
			spit8(0xC6, programCounter);
			programCounter++;
			break;
		case T_ACI:
			spit8(0xCE, programCounter);
			programCounter++;
			break;
		case T_OUT:
			spit8(0xD3, programCounter);
			programCounter++;
			break;
		case T_SUI:
			spit8(0xD6, programCounter);
			programCounter++;
			break;
		case T_IN:
			spit8(0xDB, programCounter);
			programCounter++;
			break;
		case T_SBI:
			spit8(0xDE, programCounter);
			programCounter++;
			break;
		case T_ANI:
			spit8(0xE6, programCounter);
			programCounter++;
			break;
		case T_XRI:
			spit8(0xEE, programCounter);
			programCounter++;
			break;
		case T_ORI:
			spit8(0xF6, programCounter);
			programCounter++;
			break;
		case T_CPI:
			spit8(0xFE, programCounter);
			programCounter++;
			break;
	}	
	lex(buffer, buffer_counter, read_token);
	parseState105A(buffer, buffer_counter, read_token);
}

void typeFiveHandling(char *buffer, int *buffer_counter, struct token *read_token)	//T_MVI
{
	lex(buffer, buffer_counter, read_token);
	parseState107(buffer, buffer_counter, read_token);
}

void typeSixHandling(char *buffer, int *buffer_counter, struct token *read_token)	//One operand, three byte instructions
{
	switch(read_token->type)
	{
		case T_STA:
			spit8(0x32, programCounter);
			programCounter++;
			break;
		case T_LDA:
			spit8(0x3A, programCounter);
			programCounter++;
			break;
		case T_JNZ:
			spit8(0xC2, programCounter);
			programCounter++;
			break;
		case T_JMP:
			spit8(0xC3, programCounter);
			programCounter++;
			break;
		case T_CNZ:
			spit8(0xC4, programCounter);
			programCounter++;
			break;
		case T_JZ:
			spit8(0xCA, programCounter);
			programCounter++;
			break;
		case T_CZ:
			spit8(0xCC, programCounter);
			programCounter++;
			break;
		case T_CALL:
			spit8(0xCD, programCounter);
			programCounter++;
			break;
		case T_JNC:
			spit8(0xD2, programCounter);
			programCounter++;
			break;
		case T_CNC:
			spit8(0xD4, programCounter);
			programCounter++;
			break;
		case T_JC:
			spit8(0xDA, programCounter);
			programCounter++;
			break;
		case T_CC:
			spit8(0xDC, programCounter);
			programCounter++;
			break;
		case T_JPO:
			spit8(0xE2, programCounter);
			programCounter++;
			break;
		case T_CPO:
			spit8(0xE4, programCounter);
			programCounter++;
			break;
		case T_JPE:
			spit8(0xEA, programCounter);
			programCounter++;
			break;
		case T_CPE:
			spit8(0xEC, programCounter);
			programCounter++;
			break;
		case T_JP:
			spit8(0xF2, programCounter);
			programCounter++;
			break;
		case T_CP:
			spit8(0xF4, programCounter);
			programCounter++;
			break;
		case T_JM:
			spit8(0xFA, programCounter);
			programCounter++;
			break;
		case T_CM:
			spit8(0xFC, programCounter);
			programCounter++;
			break;
		case T_SHLD:
			spit8(0x22, programCounter);
			programCounter++;
			break;
		case T_LHLD:
			spit8(0x2A, programCounter);
			programCounter++;
			break;
	}
	lex(buffer, buffer_counter, read_token);
	parseState105(buffer, buffer_counter, read_token);
}

void typeSevenHandling (char *buffer, int *buffer_counter, struct token *read_token)	//Two operand, three byte instructions
{
	lex(buffer, buffer_counter, read_token);
	parseState106(buffer, buffer_counter, read_token);	
}

void opcodeProcessing(char *buffer, int *buffer_counter, struct token *read_token)	
{
	if (read_token->type >= T_NOP && read_token->type <= T_RET)	//Opcode is single byte, no operand type
	{
		typeOneHandling(read_token);	//Handles single byte, no operand type opcodes
		if(read_token->type == T_ERROR)
			return;
		lex(buffer, buffer_counter, read_token);
		parseState6(buffer, buffer_counter, read_token);	//Expects T_EOL or T_SEMICOLON
		return;				//Exit the function
	}
	if(read_token->type >= T_STAX && read_token->type <= T_RST)	//Opcode is single byte, one operand type
	{
		typeTwoHandling(buffer, buffer_counter, read_token);
		if(read_token->type == T_ERROR)
			return;
		lex(buffer, buffer_counter, read_token);
		parseState6(buffer, buffer_counter, read_token);
		return;
	}
	if(read_token->type == T_MOV)	//Single byte, two operand opcode
	{
		typeThreeHandling(buffer, buffer_counter, read_token);
		if(read_token->type == T_ERROR)
			return;
		lex(buffer, buffer_counter, read_token);
		parseState6(buffer, buffer_counter, read_token);
		return;
	}
	if(read_token->type >= T_ADI && read_token->type <= T_CPI)	//One operand, two byte opcode
	{
		typeFourHandling(buffer, buffer_counter, read_token);
		if(read_token->type == T_ERROR)
			return;
		lex(buffer, buffer_counter, read_token);
		parseState6(buffer, buffer_counter, read_token);
		return;
	}
	if(read_token->type == T_MVI)	//Two operand, two byte opcode
	{
		typeFiveHandling(buffer, buffer_counter, read_token);
		if(read_token->type == T_ERROR)
			return;
		lex(buffer, buffer_counter, read_token);
		parseState6(buffer, buffer_counter, read_token);
		return;
	}
	if(read_token->type >= T_STA && read_token->type <= T_LHLD)	//One operand, three byte opcode
	{
		typeSixHandling(buffer, buffer_counter, read_token);
		if(read_token->type == T_ERROR)
			return;
		lex(buffer, buffer_counter, read_token);
		parseState6(buffer, buffer_counter, read_token);
		return;
	}
	if(read_token->type == T_LXI)	//Two operand, three byte opcode
	{
		typeSevenHandling(buffer, buffer_counter, read_token);
		if(read_token->type == T_ERROR)
			return;
		lex(buffer, buffer_counter, read_token);
		parseState6(buffer, buffer_counter, read_token);
		return;
	}
}

void parseState1(char *buffer, int *buffer_counter, struct token *read_token)	//T_LABEL at beginning of line
{
	if(isOpcode(read_token->type))	
	{
		lex(buffer, buffer_counter, read_token);	//Get next token
		opcodeProcessing(buffer, buffer_counter, read_token);	//Go to opcode processing
		return;		//Exit function
	}
	switch(read_token->type)
	{
		case T_DB:
				lex(buffer, buffer_counter, read_token);
				parseState2(buffer, buffer_counter, read_token);	//Move to T_DB processing
				break;
		case T_DS:
				lex(buffer, buffer_counter, read_token);
				parseState4(buffer, buffer_counter, read_token);	//Move to T_DS processing
				break;
		case T_EOL:
				return;
				break;				//Do nothing, parser handles T_EOL in main()
		case T_SEMICOLON:
				return;
				break;				//Do nothing, parser handles T_SEMICOLON in main()
		default:	//Handle as error, unexpected token has occurred
				read_token->type = T_ERROR;
				return;
	}
}

void parseState2(char *buffer, int *buffer_counter, struct token *read_token)	//T_DB Processing
{
	unsigned long int convertedNum;
	int EQUVal;

	switch(read_token->type)
	{
		case T_NUM:
			//Sanitize
			if(strlen((read_token->label + 1)) > 3)
			{
				read_token->type = T_ERROR;
				return;
			}
			//Length of number okay, now check value
			convertedNum = stringToNumber(read_token->label, 10);
			if(convertedNum > 255)	//Number outside acceptable range
			{
				read_token->type = T_ERROR;
				return;
			}
			//Spit
			spit8((unsigned char) convertedNum, programCounter);
			programCounter++;
			break;
		case T_HEXNUM:
			//Sanitize
			if(strlen((read_token->label + 1)) > 2)
			{
				read_token->type = T_ERROR;
				return;
			}
			//Length of number okay, now check value
			convertedNum = stringToNumber(read_token->label, 16);
			if(convertedNum > 255)	//Number outside acceptable range
			{
				read_token->type = T_ERROR;
				return;
			}
			//Spit
			spit8((unsigned char) convertedNum, programCounter);
			programCounter++;
			break;
		case T_OCTNUM:
			//Sanitize
			if(strlen((read_token->label + 1)) > 3)
			{
				read_token->type = T_ERROR;
				return;
			}
			//Length of number okay, now check value
			convertedNum = stringToNumber(read_token->label, 8);
			if(convertedNum > 255)	//Number outside acceptable range
			{
				read_token->type = T_ERROR;
				return;
			}
			//Spit
			spit8((unsigned char) convertedNum, programCounter);
			programCounter++;
			break;
		case T_IDENT:
			//Look up EQU table
			EQUVal = foundInEQUTable(read_token->label);
			if(EQUVal == -1)
			{
				read_token->type = T_ERROR;
				return;
			}	
			//Found in EQU Table, proceed
			if(EQUTable[EQUVal].value > 255)	//Number outside acceptable range
			{
				read_token->type = T_ERROR;
				return;
			}
			//No problem, spit
			spit8((unsigned char) EQUTable[EQUVal].value, programCounter);
			programCounter++;
			//Spit or error, depending on whether found or not
			break;
		default:
			//Unexpected token
			read_token->type = T_ERROR;
	}
	lex(buffer, buffer_counter, read_token);	//Get new token
	parseState3(buffer, buffer_counter, read_token);	//Move to T_DB number sequence processing
}

void parseState3(char *buffer, int *buffer_counter, struct token *read_token)	//T_DB number sequence processing
{
	switch(read_token->type)
	{
		case T_EOL:
			return;
			break;	//New line is fetched in main
		case T_SEMICOLON:
			return;
			break;	//New line is fetched in main
		case T_COMMA:
			//Expect new number after this
			lex(buffer, buffer_counter, read_token);	//Fetch the next token
			parseState2(buffer, buffer_counter, read_token);	//Move back to state S2 for T_DB processing
			break;
		default:
			//Unexpected token received, treat as error
			read_token->type = T_ERROR;
			return;		//Exit function
	}
}

void parseState4(char *buffer, int *buffer_counter, struct token *read_token)		//T_DS Processing
{
	long int convertedNum;
	switch(read_token->type)
	{
		case T_NUM:
//Testing		reserveStorage();
			convertedNum = stringToNumber(read_token->label, 10);
			if((programCounter + convertedNum) > 0xFFFF)
			{
				read_token->type = T_ERROR;
				return;
			}
			programCounter += convertedNum;
			break;
		case T_HEXNUM:
//Testing		reserveStorage();
			convertedNum = stringToNumber(read_token->label, 16);
			if((programCounter + convertedNum) > 0xFFFF)
			{
				read_token->type = T_ERROR;
				return;
			}
			programCounter += convertedNum;
			break;
		case T_OCTNUM:
//Testing		reserveStorage();
			convertedNum = stringToNumber(read_token->label, 8);
			if((programCounter + convertedNum) > 0xFFFF)
			{
				read_token->type = T_ERROR;
				return;
			}
			programCounter += convertedNum;
			break;
		default:
			//Treat as error
			read_token->type = T_ERROR;
			return;
	}
	lex(buffer, buffer_counter, read_token);	//Fetch next token
	parseState6(buffer, buffer_counter, read_token);	//Expects T_EOL or T_SEMICOLON in this state
}


void parseState5(char *buffer, int *buffer_counter, struct token *read_token)	//T_ORG Processing
{
	unsigned long int convertedNum;
	switch(read_token->type)
	{
		case T_NUM:
			//Need to convert number to string
			convertedNum = stringToNumber(read_token->label, 10);
			//Compare to program counter, if NUM > PC, give an error/ignore
			if(convertedNum < programCounter || convertedNum > 64000)
			{
				read_token->type = T_ERROR;
				return;
			}
		//No error
			programCounter = convertedNum;
			break;
		case T_HEXNUM:
			//Need to convert number to string
			convertedNum = stringToNumber(read_token->label, 16);
			//Compare to program counter, if NUM > PC, give an error/ignore
			if(convertedNum < programCounter || convertedNum > 64000)
			{
				read_token->type = T_ERROR;
				return;
			}
			//No error
			programCounter = convertedNum;
			break;
		case T_OCTNUM:
			//Need to convert number to string
			convertedNum = stringToNumber(read_token->label, 8);
			//Compare to program counter, if NUM > PC, give an error/ignore
			if(convertedNum < programCounter || convertedNum > 64000)
			{
				read_token->type = T_ERROR;
				return;
			}
			//No error
			programCounter = convertedNum;
			break;
		default:
			//Treat as error
			read_token->type = T_ERROR;
			return;
			//Currently, T_IDENT after T_ORG is treated as an error. Can be fixed in future revisions
	}
	lex(buffer, buffer_counter, read_token);	//Fetch new token
	parseState6(buffer, buffer_counter, read_token);
}


void parseState6(char *buffer, int *buffer_counter, struct token *read_token)
{
	switch(read_token->type)
	{
		case T_EOL:
			return;
			break;
		case T_SEMICOLON:
			return;
			break;
		default:
			//Error, unexpected token in place of T_EOL or T_SEMICOLON
			read_token->type = T_ERROR;	//Error handling done in main()
	}
}

void parseState7(char *buffer, int *buffer_counter, struct token *read_token, char *EQUNameStorage)	
{
	switch(read_token->type)
	{
		case T_EQU:
			lex(buffer, buffer_counter, read_token);	//Fetch new token
			if(foundInEQUTable(read_token->label) != -1)	//Entry already defined, treat as error
			{
				read_token->type = T_ERROR;
				return;
			}
			//Entry not previously defined, OK to continue
			parseState8(buffer, buffer_counter, read_token, EQUNameStorage);	//Process number after EQU
			break;
		default:
			//Anything else is treated as an error
			read_token->type = T_ERROR;
			return;
	}
	lex(buffer, buffer_counter, read_token);
	parseState6(buffer, buffer_counter, read_token);
}

void parseState8(char *buffer, int *buffer_counter, struct token *read_token, char *EQUNameStorage)	//Expects number after T_EQU
{
	switch(read_token->type)
	{
		case T_NUM:
			setAtEQUIndex(read_token->label, EQUNameStorage, T_NUM);
			break;
		case T_HEXNUM:
			setAtEQUIndex(read_token->label, EQUNameStorage, T_HEXNUM);
			break;
		case T_OCTNUM:
			setAtEQUIndex(read_token->label, EQUNameStorage, T_OCTNUM);
			break;
	}
	lex(buffer, buffer_counter, read_token);
	parseState6(buffer, buffer_counter, read_token);	//Expect T_EOL or T_SEMICOLON after this
}

void parseState101A(char *buffer, int *buffer_counter, struct token *read_token)	//After T_STAX or T_LDAX
{
	switch(read_token->type)
	{	
		case T_REGB:
			switch(tokenStackPop())		//Pop token out of stack and check its value
			{
				case T_STAX:
					spit8(0x02, programCounter);	
					programCounter++;
					break;
				case T_LDAX:
					spit8(0x0A, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGD:
			switch(tokenStackPop())		//Pop token out of stack and check its value
			{
				case T_STAX:
					spit8(0x12, programCounter);	
					programCounter++;
					break;
				case T_LDAX:
					spit8(0x1A, programCounter);
					programCounter++;
					break;
			}
			break;
		default:
			//Unexpected token, treat as error
			read_token->type = T_ERROR;
			return;
	}
}


void parseState101B(char *buffer, int *buffer_counter, struct token *read_token)	//After T_INX, T_DAD, T_DCX
{
	switch(read_token->type)
	{
		case T_REGB:
			switch(tokenStackPop())
			{
				case T_INX:
					spit8(0x03, programCounter);
					programCounter++;
					break;	
				case T_DCX:
					spit8(0x0B, programCounter);
					programCounter++;
					break;
				case T_DAD:
					spit8(0x09, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGD:
			switch(tokenStackPop())
			{
				case T_INX:
					spit8(0x13, programCounter);
					programCounter++;
					break;	
				case T_DCX:
					spit8(0x1B, programCounter);
					programCounter++;
					break;
				case T_DAD:
					spit8(0x19, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGH:
			switch(tokenStackPop())
			{
				case T_INX:
					spit8(0x23, programCounter);
					programCounter++;
					break;	
				case T_DCX:
					spit8(0x2B, programCounter);
					programCounter++;
					break;
				case T_DAD:
					spit8(0x29, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_SP:
			switch(tokenStackPop())
			{
				case T_INX:
					spit8(0x33, programCounter);
					programCounter++;
					break;	
				case T_DCX:
					spit8(0x3B, programCounter);
					programCounter++;
					break;
				case T_DAD:
					spit8(0x39, programCounter);
					programCounter++;
					break;
			}
			break;
		default:
			//Unexpected token
			read_token->type = T_ERROR;
			return;
	}
}

void parseState101C(char *buffer, int *buffer_counter, struct token *read_token)	//After T_PUSH, T_POP
{
	switch(read_token->type)
	{
		case T_REGB:
			switch(tokenStackPop())
			{
				case T_PUSH:
					spit8(0xC5, programCounter);
					programCounter++;
					break;
				case T_POP:
					spit8(0xC1, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGD:
			switch(tokenStackPop())
			{
				case T_PUSH:
					spit8(0xD5, programCounter);
					programCounter++;
					break;
				case T_POP:
					spit8(0xD1, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGH:
			switch(tokenStackPop())
			{
				case T_PUSH:
					spit8(0xE5, programCounter);
					programCounter++;
					break;
				case T_POP:
					spit8(0xE1, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_PSW:
			switch(tokenStackPop())
			{
				case T_PUSH:
					spit8(0xF5, programCounter);
					programCounter++;
					break;
				case T_POP:
					spit8(0xF1, programCounter);
					programCounter++;
					break;
			}
			break;
		default:
			//Error handling
			read_token->type = T_ERROR;
			return;
	}
}

void parseState101D(char *buffer, int *buffer_counter, struct token *read_token)	//After T_INR, T_DCR
{
	switch(read_token->type)
	{
		case T_REGA:
			switch(tokenStackPop())
			{
				case T_INR:
					spit8(0x3C, programCounter);
					programCounter++;
					break;
				case T_DCR:
					spit8(0x3D, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGB:
			switch(tokenStackPop())
			{
				case T_INR:
					spit8(0x04, programCounter);
					programCounter++;
					break;
				case T_DCR:
					spit8(0x05, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGC:
			switch(tokenStackPop())
			{
				case T_INR:
					spit8(0x0C, programCounter);
					programCounter++;
					break;
				case T_DCR:
					spit8(0x0D, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGD:
			switch(tokenStackPop())
			{
				case T_INR:
					spit8(0x14, programCounter);
					programCounter++;
					break;
				case T_DCR:
					spit8(0x15, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGE:
			switch(tokenStackPop())
			{
				case T_INR:
					spit8(0x1C, programCounter);
					programCounter++;
					break;
				case T_DCR:
					spit8(0x1D, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGH:
			switch(tokenStackPop())
			{
				case T_INR:
					spit8(0x24, programCounter);
					programCounter++;
					break;
				case T_DCR:
					spit8(0x25, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGL:
			switch(tokenStackPop())
			{
				case T_INR:
					spit8(0x2C, programCounter);
					programCounter++;
					break;
				case T_DCR:
					spit8(0x2D, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_M:
			switch(tokenStackPop())
			{
				case T_INR:
					spit8(0x34, programCounter);
					programCounter++;
					break;
				case T_DCR:
					spit8(0x35, programCounter);
					programCounter++;
					break;
			}
			break;
		default:
			read_token->type = T_ERROR;
			return;
	}
}

void parseState103(char *buffer, int *buffer_counter, struct token *read_token)	//After T_ADC, T_ADD, T_SUB, T_SBB, T_ANA, T_XRA, T_ORA, T_CMP
{
	switch(read_token->type)
	{
		case T_REGA:
			switch(tokenStackPop())
			{
				case T_ADD:
					spit8(0x87, programCounter);
					programCounter++;
					break;
				case T_ADC:
					spit8(0x8F, programCounter);
					programCounter++;
					break;
				case T_SUB:
					spit8(0x97, programCounter);
					programCounter++;
					break;
				case T_SBB:
					spit8(0x9F, programCounter);
					programCounter++;
					break;
				case T_ANA:
					spit8(0xA7, programCounter);
					programCounter++;
					break;
				case T_XRA:
					spit8(0xAF, programCounter);
					programCounter++;
					break;
				case T_ORA:
					spit8(0xB7, programCounter);
					programCounter++;
					break;
				case T_CMP:
					spit8(0xBF, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGB:
			switch(tokenStackPop())
			{
				case T_ADD:
					spit8(0x80, programCounter);
					programCounter++;
					break;
				case T_ADC:
					spit8(0x88, programCounter);
					programCounter++;
					break;
				case T_SUB:
					spit8(0x90, programCounter);
					programCounter++;
					break;
				case T_SBB:
					spit8(0x98, programCounter);
					programCounter++;
					break;
				case T_ANA:
					spit8(0xA0, programCounter);
					programCounter++;
					break;
				case T_XRA:
					spit8(0xA8, programCounter);
					programCounter++;
					break;
				case T_ORA:
					spit8(0xB0, programCounter);
					programCounter++;
					break;
				case T_CMP:
					spit8(0xB8, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGC:
			switch(tokenStackPop())
			{
				case T_ADD:
					spit8(0x81, programCounter);
					programCounter++;
					break;
				case T_ADC:
					spit8(0x89, programCounter);
					programCounter++;
					break;
				case T_SUB:
					spit8(0x91, programCounter);
					programCounter++;
					break;
				case T_SBB:
					spit8(0x99, programCounter);
					programCounter++;
					break;
				case T_ANA:
					spit8(0xA1, programCounter);
					programCounter++;
					break;
				case T_XRA:
					spit8(0xA9, programCounter);
					programCounter++;
					break;
				case T_ORA:
					spit8(0xB1, programCounter);
					programCounter++;
					break;
				case T_CMP:
					spit8(0xB9, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGD:
			switch(tokenStackPop())
			{
				case T_ADD:
					spit8(0x82, programCounter);
					programCounter++;
					break;
				case T_ADC:
					spit8(0x8A, programCounter);
					programCounter++;
					break;
				case T_SUB:
					spit8(0x92, programCounter);
					programCounter++;
					break;
				case T_SBB:
					spit8(0x9A, programCounter);
					programCounter++;
					break;
				case T_ANA:
					spit8(0xA2, programCounter);
					programCounter++;
					break;
				case T_XRA:
					spit8(0xAA, programCounter);
					programCounter++;
					break;
				case T_ORA:
					spit8(0xB2, programCounter);
					programCounter++;
					break;
				case T_CMP:
					spit8(0xBA, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGE:
			switch(tokenStackPop())
			{
				case T_ADD:
					spit8(0x83, programCounter);
					programCounter++;
					break;
				case T_ADC:
					spit8(0x8B, programCounter);
					programCounter++;
					break;
				case T_SUB:
					spit8(0x93, programCounter);
					programCounter++;
					break;
				case T_SBB:
					spit8(0x9B, programCounter);
					programCounter++;
					break;
				case T_ANA:
					spit8(0xA3, programCounter);
					programCounter++;
					break;
				case T_XRA:
					spit8(0xAB, programCounter);
					programCounter++;
					break;
				case T_ORA:
					spit8(0xB3, programCounter);
					programCounter++;
					break;
				case T_CMP:
					spit8(0xBB, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGH:
			switch(tokenStackPop())
			{
				case T_ADD:
					spit8(0x84, programCounter);
					programCounter++;
					break;
				case T_ADC:
					spit8(0x8C, programCounter);
					programCounter++;
					break;
				case T_SUB:
					spit8(0x94, programCounter);
					programCounter++;
					break;
				case T_SBB:
					spit8(0x9C, programCounter);
					programCounter++;
					break;
				case T_ANA:
					spit8(0xA4, programCounter);
					programCounter++;
					break;
				case T_XRA:
					spit8(0xAC, programCounter);
					programCounter++;
					break;
				case T_ORA:
					spit8(0xB4, programCounter);
					programCounter++;
					break;
				case T_CMP:
					spit8(0xBC, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGL:
			switch(tokenStackPop())
			{
				case T_ADD:
					spit8(0x85, programCounter);
					programCounter++;
					break;
				case T_ADC:
					spit8(0x8D, programCounter);
					programCounter++;
					break;
				case T_SUB:
					spit8(0x95, programCounter);
					programCounter++;
					break;
				case T_SBB:
					spit8(0x9D, programCounter);
					programCounter++;
					break;
				case T_ANA:
					spit8(0xA5, programCounter);
					programCounter++;
					break;
				case T_XRA:
					spit8(0xAD, programCounter);
					programCounter++;
					break;
				case T_ORA:
					spit8(0xB5, programCounter);
					programCounter++;
					break;
				case T_CMP:
					spit8(0xBD, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_M:
			switch(tokenStackPop())
			{
				case T_ADD:
					spit8(0x86, programCounter);
					programCounter++;
					break;
				case T_ADC:
					spit8(0x8E, programCounter);
					programCounter++;
					break;
				case T_SUB:
					spit8(0x96, programCounter);
					programCounter++;
					break;
				case T_SBB:
					spit8(0x9E, programCounter);
					programCounter++;
					break;
				case T_ANA:
					spit8(0xA6, programCounter);
					programCounter++;
					break;
				case T_XRA:
					spit8(0xAE, programCounter);
					programCounter++;
					break;
				case T_ORA:
					spit8(0xB6, programCounter);
					programCounter++;
					break;
				case T_CMP:
					spit8(0xBE, programCounter);
					programCounter++;
					break;
			}
			break;
		default:
			//Error
			read_token->type = T_ERROR;
			return;
	}
}

void parseState102(char *buffer, int *buffer_counter, struct token *read_token)		//After T_RST
{
	int convertedNum;

	if(read_token->type != T_NUM)
	{
		read_token->type = T_ERROR;
		return;	
	}
	//Token is of type T_NUM, procede with processing
	convertedNum = stringToNumber(read_token->label, 10);
	switch(convertedNum)	
	{
		case 1:
			spit8(0xCF, programCounter);
			programCounter++;
			break;
		case 2:
			spit8(0xD7, programCounter);
			programCounter++;
			break;
		case 3:
			spit8(0xDF, programCounter);
			programCounter++;
			break;
		case 4:
			spit8(0xE7, programCounter);
			programCounter++;
			break;
		case 5:
			spit8(0xEF, programCounter);
			programCounter++;
			break;
		case 6:
			spit8(0xF7, programCounter);
			programCounter++;
			break;
		case 7:
			spit8(0xFF, programCounter);
			programCounter++;
			break;
		default:
			//Treat as error
			read_token->type = T_ERROR;
			return;
	}
	lex(buffer, buffer_counter, read_token);
	parseState6(buffer, buffer_counter, read_token);
}

void parseState103A(char *buffer, int *buffer_counter, struct token *read_token)
{
	if(read_token->type >= T_REGA && read_token->type <= T_M)
	{
		tokenStackPush(*read_token);
		lex(buffer, buffer_counter, read_token);
		if(read_token->type != T_COMMA)
		{
			//Treat as error
			read_token->type = T_ERROR;
			tokenStackPop();
			return;
		}
		//T_COMMA encountered, proceed normally
		lex(buffer, buffer_counter, read_token);
		parseState104(buffer, buffer_counter, read_token);
		return;
	}
	//Token not a valid register/memory location, treat as error
	read_token->type = T_ERROR;
}

void parseState104(char *buffer, int *buffer_counter, struct token *read_token)
{
	switch(read_token->type)
	{
		case T_REGA:
			switch(tokenStackPop())
			{
				case T_REGA:
					spit8(0x7F, programCounter);
					programCounter++;
					break;
				case T_REGB:
					spit8(0x47, programCounter);
					programCounter++;
					break;
				case T_REGC:
					spit8(0x4F, programCounter);
					programCounter++;
					break;
				case T_REGD:
					spit8(0x57, programCounter);
					programCounter++;
					break;
				case T_REGE:
					spit8(0x5F, programCounter);
					programCounter++;
					break;
				case T_REGH:
					spit8(0x67, programCounter);
					programCounter++;
					break;
				case T_REGL:
					spit8(0x6F, programCounter);
					programCounter++;
					break;
				case T_M:
					spit8(0x77, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGB:
			switch(tokenStackPop())
			{
				case T_REGA:
					spit8(0x78, programCounter);
					programCounter++;
					break;
				case T_REGB:
					spit8(0x40, programCounter);
					programCounter++;
					break;
				case T_REGC:
					spit8(0x48, programCounter);
					programCounter++;
					break;
				case T_REGD:
					spit8(0x50, programCounter);
					programCounter++;
					break;
				case T_REGE:
					spit8(0x58, programCounter);
					programCounter++;
					break;
				case T_REGH:
					spit8(0x60, programCounter);
					programCounter++;
					break;
				case T_REGL:
					spit8(0x68, programCounter);
					programCounter++;
					break;
				case T_M:
					spit8(0x70, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGC:
			switch(tokenStackPop())
			{
				case T_REGA:
					spit8(0x79, programCounter);
					programCounter++;
					break;
				case T_REGB:
					spit8(0x41, programCounter);
					programCounter++;
					break;
				case T_REGC:
					spit8(0x49, programCounter);
					programCounter++;
					break;
				case T_REGD:
					spit8(0x51, programCounter);
					programCounter++;
					break;
				case T_REGE:
					spit8(0x59, programCounter);
					programCounter++;
					break;
				case T_REGH:
					spit8(0x61, programCounter);
					programCounter++;
					break;
				case T_REGL:
					spit8(0x69, programCounter);
					programCounter++;
					break;
				case T_M:
					spit8(0x71, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGD:
			switch(tokenStackPop())
			{
				case T_REGA:
					spit8(0x7A, programCounter);
					programCounter++;
					break;
				case T_REGB:
					spit8(0x42, programCounter);
					programCounter++;
					break;
				case T_REGC:
					spit8(0x4A, programCounter);
					programCounter++;
					break;
				case T_REGD:
					spit8(0x52, programCounter);
					programCounter++;
					break;
				case T_REGE:
					spit8(0x5A, programCounter);
					programCounter++;
					break;
				case T_REGH:
					spit8(0x62, programCounter);
					programCounter++;
					break;
				case T_REGL:
					spit8(0x6A, programCounter);
					programCounter++;
					break;
				case T_M:
					spit8(0x72, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGE:
			switch(tokenStackPop())
			{
				case T_REGA:
					spit8(0x7B, programCounter);
					programCounter++;
					break;
				case T_REGB:
					spit8(0x43, programCounter);
					programCounter++;
					break;
				case T_REGC:
					spit8(0x4B, programCounter);
					programCounter++;
					break;
				case T_REGD:
					spit8(0x53, programCounter);
					programCounter++;
					break;
				case T_REGE:
					spit8(0x5B, programCounter);
					programCounter++;
					break;
				case T_REGH:
					spit8(0x63, programCounter);
					programCounter++;
					break;
				case T_REGL:
					spit8(0x6B, programCounter);
					programCounter++;
					break;
				case T_M:
					spit8(0x73, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGH:
			switch(tokenStackPop())
			{
				case T_REGA:
					spit8(0x7C, programCounter);
					programCounter++;
					break;
				case T_REGB:
					spit8(0x44, programCounter);
					programCounter++;
					break;
				case T_REGC:
					spit8(0x4C, programCounter);
					programCounter++;
					break;
				case T_REGD:
					spit8(0x54, programCounter);
					programCounter++;
					break;
				case T_REGE:
					spit8(0x5C, programCounter);
					programCounter++;
					break;
				case T_REGH:
					spit8(0x64, programCounter);
					programCounter++;
					break;
				case T_REGL:
					spit8(0x6C, programCounter);
					programCounter++;
					break;
				case T_M:
					spit8(0x74, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_REGL:
			switch(tokenStackPop())
			{
				case T_REGA:
					spit8(0x7D, programCounter);
					programCounter++;
					break;
				case T_REGB:
					spit8(0x45, programCounter);
					programCounter++;
					break;
				case T_REGC:
					spit8(0x4D, programCounter);
					programCounter++;
					break;
				case T_REGD:
					spit8(0x55, programCounter);
					programCounter++;
					break;
				case T_REGE:
					spit8(0x5D, programCounter);
					programCounter++;
					break;
				case T_REGH:
					spit8(0x65, programCounter);
					programCounter++;
					break;
				case T_REGL:
					spit8(0x6D, programCounter);
					programCounter++;
					break;
				case T_M:
					spit8(0x75, programCounter);
					programCounter++;
					break;
			}
			break;
		case T_M:
			switch(tokenStackPop())
			{
				case T_REGA:
					spit8(0x7E, programCounter);
					programCounter++;
					break;
				case T_REGB:
					spit8(0x46, programCounter);
					programCounter++;
					break;
				case T_REGC:
					spit8(0x4E, programCounter);
					programCounter++;
					break;
				case T_REGD:
					spit8(0x56, programCounter);
					programCounter++;
					break;
				case T_REGE:
					spit8(0x5E, programCounter);
					programCounter++;
					break;
				case T_REGH:
					spit8(0x66, programCounter);
					programCounter++;
					break;
				case T_REGL:
					spit8(0x6E, programCounter);
					programCounter++;
					break;
			}
			break;
		default:
			//Error
			read_token->type = T_ERROR;
			return;
	}	
}

void parseState105(char *buffer, int *buffer_counter, struct token *read_token)		//Expects ADR16
{
	unsigned long int convertedNum;
	switch(read_token->type)
	{
		case T_NUM:
			//Sanitize
			if(strlen((read_token->label + 1)) > 5)
			{
				read_token->type = T_ERROR;
				return;
			}
			//Length of number okay, now check value
			convertedNum = stringToNumber(read_token->label, 10);
			if(convertedNum > 65535)	//Number outside acceptable range
			{
				read_token->type = T_ERROR;
				return;
			}
			//Spit
			spit16((unsigned short int) convertedNum, programCounter);
			programCounter += 2;
			break;
		case T_HEXNUM:
			//Sanitize
			if(strlen((read_token->label + 1)) > 4)
			{
				read_token->type = T_ERROR;
				return;
			}
			//Length of number okay, now check value
			convertedNum = stringToNumber(read_token->label, 16);
			if(convertedNum > 65535)	//Number outside acceptable range
			{
				read_token->type = T_ERROR;
				return;
			}
			//Spit
			spit16((unsigned short int) convertedNum, programCounter);
			programCounter += 2;
			break;
		case T_OCTNUM:
			//Sanitize
			if(strlen((read_token->label + 1)) > 6)
			{
				read_token->type = T_ERROR;
				return;
			}
			//Length of number okay, now check value
			convertedNum = stringToNumber(read_token->label, 8);
			if(convertedNum > 65535)	//Number outside acceptable range
			{
				read_token->type = T_ERROR;
				return;
			}
			//Spit
			spit16((unsigned short int) convertedNum, programCounter);
			programCounter += 2;
			break;
		case T_IDENT:
			//Look up symbol table
			symbolTableCall(*read_token, programCounter);
			programCounter += 2;
			//Spit or error, depending on whether found or not
			break;
		default:
			//Unexpected token
			read_token->type = T_ERROR;
	}
}

void parseState105A(char *buffer, int *buffer_counter, struct token *read_token)	//Expects D8
{
	unsigned long int convertedNum;
	int EQUVal;

	switch(read_token->type)
	{
		case T_NUM:
			//Sanitize
			if(strlen((read_token->label + 1)) > 3)
			{
				read_token->type = T_ERROR;
				return;
			}
			//Length of number okay, now check value
			convertedNum = stringToNumber(read_token->label, 10);
			if(convertedNum > 255)	//Number outside acceptable range
			{
				read_token->type = T_ERROR;
				return;
			}
			//Spit
			spit8((unsigned char) convertedNum, programCounter);
			programCounter++;
			break;
		case T_HEXNUM:
			//Sanitize
			if(strlen((read_token->label + 1)) > 2)
			{
				read_token->type = T_ERROR;
				return;
			}
			//Length of number okay, now check value
			convertedNum = stringToNumber(read_token->label, 16);
			if(convertedNum > 255)	//Number outside acceptable range
			{
				read_token->type = T_ERROR;
				return;
			}
			//Spit
			spit8((unsigned char) convertedNum, programCounter);
			programCounter++;
			break;
		case T_OCTNUM:
			//Sanitize
			if(strlen((read_token->label + 1)) > 3)
			{
				read_token->type = T_ERROR;
				return;
			}
			//Length of number okay, now check value
			convertedNum = stringToNumber(read_token->label, 8);
			if(convertedNum > 255)	//Number outside acceptable range
			{
				read_token->type = T_ERROR;
				return;
			}
			//Spit
			spit8((unsigned char) convertedNum, programCounter);
			programCounter++;
			break;
		case T_IDENT:
			//Look up EQU table
			EQUVal = foundInEQUTable(read_token->label);
			if(EQUVal == -1)
			{
				read_token->type = T_ERROR;
				return;
			}	
			//Found in EQU Table, proceed
			if(EQUTable[EQUVal].value > 255)	//Number outside acceptable range
			{
				read_token->type = T_ERROR;
				return;
			}
			//No problem, spit
			spit8((unsigned char) EQUTable[EQUVal].value, programCounter);
			programCounter++;
			//Spit or error, depending on whether found or not
			break;
		default:
			//Unexpected token
			read_token->type = T_ERROR;
	}
}

void parseState106(char *buffer, int *buffer_counter, struct token *read_token)	
{
	switch(read_token->type)
	{
		case T_REGB:
			spit8(0x01, programCounter);
			programCounter++;
			break;
		case T_REGD:
			spit8(0x11, programCounter);
			programCounter++;
			break;
		case T_REGH:
			spit8(0x21, programCounter);
			programCounter++;
			break;
		case T_SP:
			spit8(0x31, programCounter);
			programCounter++;
			break;
		default:
			//Unexpected token
			read_token->type = T_ERROR;
			return;
	}
	lex(buffer, buffer_counter, read_token);
	if(read_token->type != T_COMMA)
	{
		read_token->type = T_ERROR;
		return;
	}
	lex(buffer, buffer_counter, read_token);
	parseState105(buffer, buffer_counter, read_token);
}

void parseState107(char *buffer, int *buffer_counter, struct token *read_token)
{
	switch(read_token->type)
	{
		case T_REGA:
			spit8(0x3E, programCounter);
			programCounter++;
			break;
		case T_REGB:
			spit8(0x06, programCounter);
			programCounter++;
			break;
		case T_REGC:
			spit8(0x0E, programCounter);
			programCounter++;
			break;
		case T_REGD:
			spit8(0x16, programCounter);
			programCounter++;
			break;
		case T_REGE:
			spit8(0x1E, programCounter);
			programCounter++;
			break;
		case T_REGH:
			spit8(0x26, programCounter);
			programCounter++;
			break;
		case T_REGL:
			spit8(0x2E, programCounter);
			programCounter++;
			break;
		case T_M:
			spit8(0x36, programCounter);
			programCounter++;
			break;
		default:
			//Unexpected token
			read_token->type = T_ERROR;
			return;
	}
	lex(buffer, buffer_counter, read_token);
	if(read_token->type != T_COMMA)
	{
		//Comma not encountered, treat as error
		read_token->type = T_ERROR;
		return;
	}
	//Comma encountered, proceed normally
	lex(buffer, buffer_counter, read_token);
	parseState105A(buffer, buffer_counter, read_token);	//Expects D8
}

int isOpcode(int tokenType)		//Accepts token type, returns 1 if it is an opcode, else 0
{
	if (tokenType >= T_NOP && tokenType <= T_LXI)	//It is of type T_OPCODE
		return 1;
	//Not of type T_OPCODE
	return 0;
}



void setAtEQUIndex(char *EQULabel, char *EQUNameStorage, int type)
{
	switch(type)
	{
		case T_NUM:
			EQUTable[EQUIndex].value = (int) stringToNumber(EQULabel, 10);
			strcpy(EQUTable[EQUIndex].name, EQUNameStorage);
			EQUIndex++;
			break;
		case T_HEXNUM:
			EQUTable[EQUIndex].value = (int) stringToNumber(EQULabel, 16);
			strcpy(EQUTable[EQUIndex].name, EQUNameStorage);
			EQUIndex++;
			break;
		case T_OCTNUM:
			EQUTable[EQUIndex].value = (int) stringToNumber(EQULabel, 8);
			strcpy(EQUTable[EQUIndex].name, EQUNameStorage);
			EQUIndex++;
			break;
	}
}

int foundInEQUTable(char *EQULabel)
{
	int counter = 0;
	while(counter < EQUIndex)
	{
		if(strcmp(EQULabel, EQUTable[counter].name) == 0)
			return counter;
		counter++;
	}
	//Not found
	return -1;
}


void tokenStackPush(struct token read_token)		//Pushes the token into the stack
{
	if (tokenStackTop == TOKENSTACKSIZE - 1)	
	{
		//Stack is full, give error message and return. Perhaps mark as error and move to next line?
		printf("\nStack is full, unable to push (Line %d)", line_counter);
		return;
	}
	//Stack is not full, procede to push element into stack
	tokenStackTop++;	
	//Copy over corresponding elements of struct token
	strcpy(tokenStack[tokenStackTop].label, read_token.label);
	tokenStack[tokenStackTop].type = read_token.type;
}

int tokenStackPop()					//Pops the token out of the stack, returns the token type of popped token
{
	if(tokenStackTop == -1)
	{	
		//Stack is empty, give error message and escape
		printf("\nStack is empty, unable to pop (Line %d)", line_counter);
		return;
	}
	//Stack is not empty, procede to pop topmost token
	tokenStackTop--;
	//Return type of topmost element
	return tokenStack[tokenStackTop + 1].type;
}

void spit8(unsigned char output, int programCounter)	//Writes one byte to memory
{
	*(baseAddress + programCounter) = output;	
}

void spit16(unsigned short int output, int programCounter)	//Writes two bytes to memory
{
	*((unsigned short int *)(baseAddress + programCounter)) = output;
}

void dumpMemoryToFile(FILE *destination)
{
	unsigned int counter = 0;
	while(counter < MEMORYSIZE)
	{
		fwrite((baseAddress + counter), 1, 1, destination);	//Writes to file one byte at a time
		counter++;
	}
}
