IDIR =./
CC=cc
CFLAGS=-g

ODIR=./
LDIR =./

LIBS=

_DEPS = lex.h tokens.h symTab.h mylib.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_DEPS1 = inclusions.h
DEPS1 = $(patsubst %,$(IDIR)/%,$(_DEPS1))

_OBJ = lex.o symTab.o parser.o mylib.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

_OBJ1 = disassembler.o
OBJ1 = $(patsubst %,$(ODIR)/%,$(_OBJ1))

all: asm85 dasm85

$(ODIR)/%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

asm85: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

dasm85: $(OBJ1)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)


.PHONY: clean

clean:
	rm -f $(ODIR)/*.o core $(ODIR)/asm85 $(ODIR)/dasm85
