# README #

An assembler, disassembler and emulator for Intel's 8085 microprocessor.

### What is this repository for? ###

* Assembles 8085 code to binary
* Disassembles binary to corresponding 8085 assembly
* Picks up assembled code and executes it in an emulated environment (yet to be tested)

### How do I get set up? ###

* Run 'make all'
* To assemble - ./asm85 <Filename> . Assembled code is written to the file 'assembled'
* To disassemble - ./dasm85 <Filename> .

### Who do I talk to? ###

* Mayank Bhatt - mayankbh@gmail.com