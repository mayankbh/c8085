#include <stdio.h>		//Standard header for I/O
#include "./inclusions.h"	//Contains struct declarations and opcode tables
#include <stdlib.h>

int programCounter = 0x0000;	//Stores address of instruction

int opcodeCheck(char searchTerm);	//Function to search opcode table

void main()
{


	//Opening the file


	FILE *inputFile;	//Input file pointer

	//TESTING
#if 0
	inputFile = fopen("input.bin", "wb");
	fprintf(inputFile,"%c%c%c%c%c", 0x3E, 0x8E,0x3E,0x12,0xB8);
	fclose(inputFile);
#endif
	//TESTING


	inputFile = fopen("assembled","rb");	//Opening file in input+binary mode

	if(inputFile == NULL)			//Error in opening file
	{
		printf("Error in reading file. Exiting");
		return;				//Exit program
	}


	//File opened successfully, continue execution


	unsigned char buffer;					//Used to store single byte read from file
	unsigned char buffer_additional;				//Used if one extra byte is to be read
	unsigned short int buffer_additional_1;		//ADR 16
	int index;					//Stores index of matching entry in opcode table

	while(fread(&buffer,sizeof(buffer), 1, inputFile))		//Reading from file until EOF is encountered
	{
		index = opcodeCheck(buffer);		//opcodeCheck() returns index of matching entry from table, -1 if not found

		if (index == -1)		//No match
		{
			printf("Unknown opcode encountered. Exiting");
			fclose(inputFile);
			return;
		}	
		else				//Match found
		{
			if(opcodeTable[index].size == 0)		//No additional bytes to be read
			{
				printf("\n%04X %s", programCounter, opcodeTable[index].mnemonic);	//Prints mnemonic 
				if(opcodeTable[index].nargs == 1)
					printf(" %s", opcodeTable[index].arg1);
				if(opcodeTable[index].nargs == 2)
					printf(" %s,%s", opcodeTable[index].arg1, opcodeTable[index].arg2);
				programCounter += 1;
			}
			else if(opcodeTable[index].size == 1)		//Single extra byte to be read
			{
				fread(&buffer_additional, sizeof(buffer_additional), 1, inputFile);	
				printf("\n%04X %s ", programCounter, opcodeTable[index].mnemonic);	//Prints mnemonic 
				if(opcodeTable[index].nargs == 1)
					printf(" %s,", opcodeTable[index].arg1); //Prints hardcoded argument
				printf(" %04X", buffer_additional);	//Prints argument read from file
				programCounter += 2;
			}
			else if(opcodeTable[index].size == 2)		//Two extra bytes to be read
			{
				fread(&buffer_additional_1, sizeof(unsigned short int), 1, inputFile);	//Reads 2 byte address
				printf("\n%04X %s", programCounter, opcodeTable[index].mnemonic);
				if(opcodeTable[index].nargs == 1)
					printf(" %s,", opcodeTable[index].arg1);
				printf(" %04X", buffer_additional_1);
				programCounter += 3;
			}
		}
	}
	
	fclose(inputFile);		//Closing input file
}

int opcodeCheck(char searchTerm)
{
	int counter;
	for(counter = 0;counter < opcodeTableSize;counter++)	//Traversing opcode table
	{
		if(searchTerm == opcodeTable[counter].opcode)	//Matching term found
		{
			return counter;		//Returning index of matching entry
		}
	}
	//Match not found
	return -1;
}
