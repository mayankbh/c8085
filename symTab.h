#ifndef SYMBOLTABLE
#define SYMBOLTABLE

#define SYMBOLTABLESIZE 500 

#define IDENT 0
#define LABEL 1

#define HASHSEED 65599

#define NAMESIZE 50

struct symbolTableEntry 
{ 
	char  name[NAMESIZE];          // name 
	unsigned short int  address;         // value once defined 
	unsigned int defined;           // true after defining occurrence encountered 
	struct symbolTableEntry  *sLink;      // to next entry 
	struct forwardRefs *fLink;  // to forward references 
};


struct forwardRefs 
{   // forward references for undefined labels 
	unsigned int addressToPatch;          // to be patched 
	struct forwardRefs *nLink;  // to next reference
};


#endif
