#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "tokens.h"
#include "symbolTable.h"
#include "lex.h"

int programCounter = 0;

//Declare symbol table as a global array, all entries intially null

struct symbolTableEntry symbolTable[SYMBOLTABLESIZE];

void strupr(char *string);
int labelNotDefined(char *label, unsigned int hashVal);	//Checks label under hash value, returns 0 if not used, 1 if used but not defined, 2 if used and defined (error case)
unsigned int hash(char *string);	//Accepts a string, generates hash from it
int hashCheck(unsigned int hashVal);	//Checks hash table for existing entry, returns 1 if no entry found, else 0
void createNewEntry(char *label, unsigned int hashVal, int entryType);	//Creates first entry under given hash value
void hashAppend(char *label, unsigned int hashVal, int entryType);	//Appends entry under given hash value
void forwardRefAppend(char *label, unsigned int hashVal);
void addressPatch(char *label, unsigned int hashVal);
void symInit(struct symbolTableEntry *symTab, int tableSize);	
void symPrint(struct symbolTableEntry *symTab, int tableSize);
int symUnresolved(struct symbolTableEntry *symTab, int tableSize, int toPrint);	//Determines number of unresolved entries in symbol table
void lex(char buffer[], int *buffer_counter, struct token *read_token);

#if 0
int main()
{
	unsigned int hashVal;	//Stores hash value generated from string	
	struct token read_token;	//Stores token read by lexer
	char buffer[81];	//Stores a single line of text read from file
	int buffer_counter = 0;
	int line_counter = 0;
	FILE *inputFile;
	

	inputFile = fopen("input", "r");
	//Handling file opening errors, if any
	if(inputFile == NULL)
	{
		printf("Error in opening file. Exiting");
		exit(1);
	}

	//Loop will run until end of file
	while(fread(buffer, 81, 1, inputFile))
	{
		//Replace line break or carriage return with null character
		while(buffer[buffer_counter] != '\n' && buffer[buffer_counter] != '\r')
			buffer_counter++;
		buffer[buffer_counter] = '\0';	
		buffer_counter = 0;
		line_counter++;
#endif

void main(int argc, char *argv[])
{
	FILE *inputFile;			//Declaring file pointer
	char buffer[BUFFERSIZE];
	int buffer_counter;			//Counter used to traverse line of text
	int line_counter;
	struct token read_token;		//Token read from string
	unsigned int hashVal;

	if(argc != 2)
	{
		printf("Usage:%s  <Input file name>\n", argv[0]);
		exit(1);
	}
	inputFile = fopen(argv[1], "r");	//Opening file in read mode
	if(inputFile == NULL)			//Error in opening file
	{
		printf("Error in opening file %s. Exiting.\n", argv[1]);
		exit(1);				//Exit program
	}

	//Initializing counters

	buffer_counter = 0;			
	line_counter = 0;

	//Reading from file
	
	while(fgets(buffer,BUFFERSIZE,inputFile) != NULL)		//Until end of file
	{
		line_counter++;			//New line starts here, increment counter
		buffer_counter = 0;		//New line, reset buffer counter here
		while(buffer[buffer_counter] != '\n' && buffer[buffer_counter] != '\r')
			buffer_counter++;
		buffer[buffer_counter] = '\0';		//Remove \n and \r
		buffer_counter = 0;
		do
		{
			//Fetch a token from the file
			lex(buffer,&buffer_counter,&read_token);
			strupr(read_token.label);	//Converting to upper case
			//Behaviour based on type of token
			switch(read_token.type)
			{
			//If it is a label
				case T_LABEL:
					//Generate hash
					hashVal = hash(read_token.label);
					//Check if the generated hash already exists in symbol table
					if(hashCheck(hashVal))
					{
						//Hash already in use, check if label is in use.
						switch(labelNotDefined(read_token.label, hashVal))
						{
							case 0: 	//Not in use
								//Append entry in hash table, set defined flag to true
								hashAppend(read_token.label, hashVal, LABEL);
//The following statement is for testing only
								programCounter+=3;	
//The preceding statement is for testing only
								break;
							case 1:		//In use, but not defined
								//Set defined flag to true, patch all addresses
								addressPatch(read_token.label, hashVal);
								break;
							case 2:		//In use, already defined
								//Label already defined, generate error and move to next token
								printf("\nLabel %s already defined.", read_token.label);
								break;
						}	
						break;	//Move to next token
					}	
					//Hash not in use, create new entry here
					createNewEntry(read_token.label, hashVal, LABEL);
//The following statement is for testing only
								programCounter+=3;	
//The preceding statement is for testing only
					break;
				case T_IDENT:
					//Generate hashs
					hashVal = hash(read_token.label);
					//Check if the generated hash already exists in symbol table
					if(hashCheck(hashVal))
					{
						//Hash already in use, check if label is in use
						switch(labelNotDefined(read_token.label, hashVal))
						{
							case 0:		//Not in use
								//Create entry, append to forward references
								hashAppend(read_token.label, hashVal, IDENT);
								break;
							case 1:		//Name used but label not defined
								//Add to forward references
								forwardRefAppend(read_token.label, hashVal);
								break;
							case 2:		//Label defined previously
								//Patch address to memory directly
								break;
						}
						break;	//Next token
					}
					//Hash not in use, create new entry here
					createNewEntry(read_token.label, hashVal, IDENT);
					break;
			}
		}	
		while (read_token.type != T_EOL && read_token.type != T_ERROR);
		//End of line encountered
	}
	//Loop will run till end of file
	symPrint(symbolTable, SYMBOLTABLESIZE);	
	symUnresolved(symbolTable, SYMBOLTABLESIZE, 1);
	//Be sure to free all the memory allocated during execution
	//deleteSymbolTable();
	
}


void strupr(char *string)	//Converts string to upper case
{
	char *sp = string;
	while(*sp != '\0')
	{
		if(*sp >= 'a' && *sp <= 'z')
			*sp -= 32;
		sp++;
	}
}

unsigned int hash(char *string)
{
	char *cp = string;
	unsigned int hashVal = 0;

	while(*cp != '\0')	//Traverse the string
	{
		hashVal = (hashVal * 65599) + *cp;
		cp++;
	}
	return (hashVal % SYMBOLTABLESIZE);
}


int hashCheck(unsigned int hashVal)
{
	if(symbolTable[hashVal].name[0] == '\0')	//Hash value not in use
		return 0;
	//Hash value in use
	return 1;
}


int labelNotDefined(char *label, unsigned int hashVal)	//Checks label under hash value, returns 0 if not used, 1 if used but not defined, 2 if used and defined (error case)
{
	struct symbolTableEntry *search;

	search = &symbolTable[hashVal];		//Address of first entry under hash value
	//This function is only called if hash collision occurs, implying first entry cannot be null
	//Hence no need to check if first entry is null
	while(search != NULL)	//Until end of linked list
	{
		if(strcmp(search->name, label) == 0)	//Match found
		{
			switch(search->defined)
			{
				case 0:		//Not defined previously
					return 1;
				case 1:		//Defined previously
					return 2;
			}
		}
		//Match not found, move to next element in list
		search = search->sLink;
	}
	//Entire list traversed, no match found. First occurrence of symbol
	return 0;
}

void createNewEntry(char *label, unsigned int hashVal, int entryType)
{
	struct forwardRefs *temp;

	strcpy(symbolTable[hashVal].name, label);	//Set name of entry
	switch(entryType)
	{
		case LABEL:
			symbolTable[hashVal].address = programCounter;	//Set address of symbol table entry
			symbolTable[hashVal].defined = 1;	//Set defined flag to true
			break;
		case IDENT:
			symbolTable[hashVal].defined = 0;
			symbolTable[hashVal].address = 0x00;	//Temporary 0 address
			temp = malloc(sizeof(struct forwardRefs));
			temp->addressToPatch = programCounter;	
			temp->nLink = NULL;	
			symbolTable[hashVal].fLink = temp;
			break;
	}
}

void hashAppend(char *label, unsigned int hashVal, int entryType)
{
	struct symbolTableEntry *traverse;	//Used to traverse linked list
	struct symbolTableEntry *temp;		//Used to append entry to linked list
	struct forwardRefs *fRef;		//Used in case of ident to generate forward reference

	traverse = &symbolTable[hashVal];	//Address of first entry under hash value
	while(traverse->sLink != NULL)			//Until end of linked list
		traverse = traverse->sLink;
	//traverse now points to last element of linked list
	temp = malloc(sizeof(struct symbolTableEntry));
	traverse->sLink = temp;		//Appending latest element to linked list
	temp->sLink = NULL;		//Terminating null pointer
	strcpy(temp->name, label);
	switch(entryType)
	{
		case LABEL:
			temp->address = programCounter;
			temp->defined = 1;
			temp->fLink = NULL;
			break;
		case IDENT:
			temp->address = 0x00;	//Temporary 0 address
			temp->defined = 0;

			fRef = malloc(sizeof(struct forwardRefs));
			fRef->addressToPatch = programCounter;
			fRef->nLink = NULL;
			temp->fLink = fRef;
			break;
	}
}

void addressPatch(char *label, unsigned int hashVal)
{
	struct symbolTableEntry *traverse;
	struct forwardRefs *fRef;

	traverse = &symbolTable[hashVal];
	while(strcmp(label, traverse->name) != 0)
	{
		traverse = traverse->sLink;	//Shift to next entry
	}
	//traverse now points to the entry to be changed
	traverse->address = programCounter;	//Setting address
	traverse->defined = 1;			//Set defined flag to true
	fRef = traverse->fLink;
	while(fRef != NULL)
	{
		//Patch address

		//Move to next element
		fRef = fRef->nLink;
	}		
}

void forwardRefAppend(char *label, unsigned int hashVal)
{
	struct symbolTableEntry *traverse = &symbolTable[hashVal];
	struct forwardRefs *fRef, *temp;

	while(strcmp(traverse->name, label) != 0)
		traverse = traverse->sLink;
	//Traverse now points to entry where forward reference must be added
	fRef = traverse->fLink;

	while(fRef->nLink != NULL)
		fRef = fRef->nLink;

	temp = malloc(sizeof(struct forwardRefs));
	temp->addressToPatch = programCounter;
	temp->nLink = NULL;
	
	fRef->nLink = temp;
}

void symInit(struct symbolTableEntry *symTab, int tableSize)
{
	int counter = 0;
	while(counter < tableSize)
	{
		symTab[counter].name[0] = '\0';
		symTab[counter].address = (unsigned int) NULL;
		symTab[counter].defined = (unsigned int) NULL;
		symTab[counter].sLink = (struct symbolTableEntry *) NULL;
		symTab[counter].fLink = (struct forwardRefs *) NULL;
		counter++;	
	}
}

//Function to display symbol table


int symUnresolved(struct symbolTableEntry *symTab, //Pointer to symbol table
			int tableSize, 		//Size of symbol table
			int toPrint)		//Whether to print unresolved entries or not
{
	int counter = 0;
	int undefCount = 0;	//Number of undefined entries
	struct forwardRefs *fRef;	//Used to traverse forward references
	struct symbolTableEntry *symEnt;	//Used to traverse all entries under given hash

	while(counter < tableSize)
	{
		if(symTab[counter].name[0] != '\0')	//Found an existing hashs
		{
			//Check if first entry is defined
			if(symTab[counter].defined == 0)	//Not defined
			{
				undefCount++;
				if(toPrint)	//Check flag
				{
					printf("\n%s referenced at \t", symTab[counter].name);
					fRef = symTab[counter].fLink;
					while(fRef != NULL)
					{
						printf("%04X ", fRef->addressToPatch);
						fRef = fRef->nLink;
					}
				}
			}				
			//Check rest of entries
			symEnt = symTab[counter].sLink;	//Points to next entry
			while(symEnt != NULL)
			{

				if(symEnt->defined == 0)	//Not defined
				{
					undefCount++;
					if(toPrint)	//Check flag
					{
						printf("\n%s referenced at \t", symEnt->name);
						fRef = symEnt->fLink;
						while(fRef != NULL)
						{
							printf("%04X ", fRef->addressToPatch);
							fRef = fRef->nLink;
						}
					}
				}				
				symEnt = symEnt->sLink;
			}
		}
		counter++;
	}	
	if(toPrint)
		printf("\n");
	return undefCount;
}

void symPrint(struct symbolTableEntry *symTab,	
			int tableSize)
{
	int counter = 0;
	struct symbolTableEntry *symEnt;	//Used to traverse all entries under given hash

	while(counter < tableSize)
	{
		if(symTab[counter].name[0] != '\0')	//Found an existing hashs
		{
			printf("\n%s\t\t%04X\t%s", symTab[counter].name, symTab[counter].address, symTab[counter].defined ? "" : "Undefined");
			//Check rest of entries
			symEnt = symTab[counter].sLink;	//Points to next entry
			while(symEnt != NULL)
			{
				printf("\n%s\t\t%04X\t%s", symEnt->name, symEnt->address, symEnt->defined ? "" : "Undefined");
				symEnt = symEnt->sLink;
			}
		}
		printf("\n---\n");
		counter++;
	}	
	printf("\n");

}
