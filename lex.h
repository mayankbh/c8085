#ifndef LEX
#define LEX

#define BUFFERSIZE 200
#define MAXLABELSIZE 20
#define MAXTOKSIZE 25

struct token			//Tokens are structures with a token type and token value
{
	int type;
	char label[MAXLABELSIZE];
};


struct tokenTable
{
	char *tokenName;
	int tokenVal;
};

#endif
